import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule,ErrorHandler,Injectable,Injector,InjectionToken } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
 
import { AdminLayoutComponent } from './layouts/admin-new/admin-layout.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import { ScrollModule } from './scroll/scroll.module';
import { LocationStrategy, PathLocationStrategy, DatePipe } from '@angular/common';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgxPermissionsModule } from 'ngx-permissions';

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { Http } from "@angular/http";
import { SessionService } from './shared/service/session.service';
import { AppService } from "./shared/service/app.service";
import * as Rollbar from 'rollbar';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "api/Language/", "");
}

const rollbarConfig = {
    accessToken: 'e18fc0365b3e4ffc994f0bbc7822b63f',
    captureUncaught: true,
    captureUnhandledRejections: true,
    hostWhiteList: ['autodesk.ligarian.com'],
    checkIgnore: function(isUncaught, args, payload) {
        return isUncaught === true;
    },
    ignoredMessages: ["TypeError:","'Invalid Date' for pipe 'DatePipe'","Cannot read property"]
};

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) { }
  handleError(err:any) : void {
    var rollbar = this.injector.get(Rollbar);
    rollbar.error(err.originalError || err);
  }
}

export function rollbarFactory(){
    return new Rollbar(rollbarConfig);
}

export const RollbarService = new InjectionToken<Rollbar>('rollbar');

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    AdminLayoutComponent,
    BreadcrumbsComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    ScrollModule,
    HttpClientModule,
    NgxPermissionsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [ScrollModule],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    SessionService, DatePipe, AppService,
    { provide: ErrorHandler, useClass: RollbarErrorHandler },
    { provide: Rollbar, useFactory: rollbarFactory },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
