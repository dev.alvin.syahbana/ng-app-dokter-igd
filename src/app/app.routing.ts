import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-new/admin-layout.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login-dokter-igd',
    pathMatch: 'full'
  },  
  // New
  {
    path: 'login-dokter-igd',
    loadChildren: './auth/login-dokter-igd/login-dokter-igd.module#LoginDokterIGDModule'
  },
  {
    path: 'list-pasien-igd',
    component: AdminLayoutComponent,
    loadChildren: './app-dokter-igd/list-pasien-igd/list-pasien-igd.module#ListPasienIGDModule',
  },
  {
    path: 'forbidden',
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  },
  {
    path: '**',
    redirectTo: 'forbidden'
  }
];
