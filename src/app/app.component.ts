import { Component } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { Router, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { SessionService } from './shared/service/session.service';

@Component({
	selector: 'app-root',
	template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class AppComponent {

	constructor(
		private permissionsService: NgxPermissionsService,
		private router: Router,
		public session: SessionService,
	) {
		const perm = ["EDITOR"];
		this.permissionsService.loadPermissions(perm);

		router.events.forEach((event: NavigationEvent) => {
			if (event instanceof NavigationEnd) {
				var url = event.url;
				var routing = url.split("/");

				//session true
				if (this.session.isLoggedIn()) {
					if (routing[1] == "") {
						// this.router.navigate(['/app-dokter/list-pasien-rawat-jalan']);
						// this.router.navigate(['menu-utama']);
						this.router.navigate(['list-pasien-igd'])
					}
				}
				else {
					if (routing[1] == "") {

					}
					else {
						this.router.navigate(["/"]);
					}
				}

			}
		});
	}
}
