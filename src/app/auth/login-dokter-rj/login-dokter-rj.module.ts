import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginDokterRJComponent } from './login-dokter-rj.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const LoginDokterRJRoutes: Routes = [
{
  path: '',
  component: LoginDokterRJComponent,
  data: {
    breadcrumb: 'Login',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}
];

@NgModule({
  imports: [
  CommonModule,
  RouterModule.forChild(LoginDokterRJRoutes),
  SharedModule,
  AngularMultiSelectModule
  ],
  declarations: [LoginDokterRJComponent]
})

export class LoginDokterRJModule { }