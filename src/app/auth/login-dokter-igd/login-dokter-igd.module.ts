import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginDokterIGDComponent } from './login-dokter-igd.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';

export const LoginDokterIGDRoutes: Routes = [
  {
    path: '',
    component: LoginDokterIGDComponent,
    data: {
      breadcrumb: 'Login',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginDokterIGDRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [LoginDokterIGDComponent]
})

export class LoginDokterIGDModule { }
