import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import {
  FormGroup,
  FormControl
} from "@angular/forms";
import {
  AppService
} from "../../shared/service/app.service";
import {
  SessionService
} from '../../shared/service/session.service';
import {
  Router
} from '@angular/router';
import { isEmpty } from 'rxjs/operator/isEmpty';

@Component({
  selector: 'Login',
  templateUrl: './login-dokter-igd.component.html',
  styleUrls: [
    './login-dokter-igd.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class LoginDokterIGDComponent implements OnInit {

  public username: string = '';
  public password: string = '';
  public dataUser: any;
  public loading = false;

  constructor(private router: Router, public session: SessionService, private service: AppService) { }

  ngOnInit() { }

  onSubmit() {
    this.loading = true;
    var d = new Date();
    var year = d.getFullYear();
    var month = '' + (d.getMonth() + 1);
    if (month.length == 1)
      month = "0" + month;
    var day = '' + d.getDate();
    if (day.length == 1)
      day = "0" + day;
    var dates = year + "-" + month + "-" + day;

    if (this.username == '' && this.password == '') {
      window.alert("Username dan Password tidak boleh kosong");
      this.loading = false;
    }
    else if (this.username == '' || this.password == '') {
      window.alert("Username atau Password tidak boleh kosong");
      this.loading = false;
    }
    else if (this.username != '' && this.password != '') {
      this.service.httpClientGet('api/AppDokterIgd/GetUserIdDOkter/' + this.username + '/' + this.password, '')
        .subscribe(result => {
          this.dataUser = result;
          if (Object.keys(this.dataUser).length === 0 && this.dataUser.constructor === Object) {
            window.alert("Username atau Password Salah");
            this.loading = false;
          }
          else {
            this.session.logIn('token', JSON.stringify({
              tgl_hari_ini: dates,
              // tgl_hari_ini: '2019-05-20', // 20101523
              // tgl_hari_ini: '2019-07-18', // 19950271
              // tgl_hari_ini: '2019-08-19', // 20050919
              kd_user: this.username,
              nama: this.dataUser.Nm_Dokter
            }));
            this.router.navigate(['list-pasien-igd']);
            this.loading = false;
            const paramLog = {
              NoReg: '',
              Kd_Dokter: this.dataUser.Kd_Dokter,
              Aksi: 'LOGIN',
              Jenis: '2'
            }
            this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });
          }
        },
          error => {
            this.loading = false;
          });
    }
  }
}