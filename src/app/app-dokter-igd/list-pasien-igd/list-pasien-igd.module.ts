import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  ListPasienIGDComponent,
  DateFormatPipe,
  TimeFormatPipe,
  DataFilterPipe,
  DataFilterPaketNonRacikPipe,
  DataFilterPaketRacikPipe,
  DataFilterRiwayatDiagnosa,
  DataFilterJenisPlyn, 
  DataFilterPemeriksaanLyn
} from './list-pasien-igd.component';
import {
  RouterModule,
  Routes
} from "@angular/router";
import {
  SharedModule
} from "../../shared/shared.module";
import {
  AppService
} from "../../shared/service/app.service";
import {
  LoadingModule
} from 'ngx-loading';
import {
  DatePipe
} from '@angular/common';
import {
  Ng2CompleterModule
} from "ng2-completer";
import {
  AngularMultiSelectModule
} from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const ListPasienIGDRoutes: Routes = [{
  path: '',
  component: ListPasienIGDComponent,
  data: {
    breadcrumb: 'List Pasien IGD',
    icon: 'icofont-home bg-c-blue',
    status: false,
    title: 'List Pasien IGD'
  }
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPasienIGDRoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule,
    AngularMultiSelectModule
  ],
  declarations: [ListPasienIGDComponent, DateFormatPipe, TimeFormatPipe, DataFilterPipe, DataFilterPaketNonRacikPipe, DataFilterPaketRacikPipe, DataFilterRiwayatDiagnosa, DataFilterJenisPlyn, DataFilterPemeriksaanLyn],
  providers: [AppService]
})
export class ListPasienIGDModule {}
