import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import {
  AppService
} from "../../shared/service/app.service";
import swal from 'sweetalert2';
import {
  SessionService
} from '../../shared/service/session.service';
import {
  Pipe,
  PipeTransform
} from '@angular/core';
import {
  DatePipe
} from '@angular/common';
import {
  FormGroup,
  FormControl,
  Validators
} from "@angular/forms";
declare var require: any;
import {
  CompleterService,
  CompleterData,
  RemoteData
} from 'ng2-completer';
import {
  NgbTabChangeEvent
} from '@ng-bootstrap/ng-bootstrap';
import {
  parse
} from 'url';

@Pipe({
  name: 'dataFilterRiwayatDiagnosa'
})
export class DataFilterRiwayatDiagnosa {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Tgl_Masuk.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Trx.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Jenis_Pelayanan.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.English_ICD.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dateFormatPipe'
})
export class DateFormatPipe implements PipeTransform {
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'dd-MM-yyyy');
    return value;
  }
}

@Pipe({
  name: 'timeFormatPipe'
})
export class TimeFormatPipe implements PipeTransform {
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'HH:mm');
    return value;
  }
}

@Pipe({
  name: 'dataFilter'
})
export class DataFilterPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Status.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.No_Karcis.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Kd_RM.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.JenKel.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Umur_Masuk.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPaketObatNonRacik'
})
export class DataFilterPaketNonRacikPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nm_Template.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPaketObatRacik'
})
export class DataFilterPaketRacikPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nm_Template.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterJenisPlyn'
})
export class DataFilterJenisPlyn {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Jenis_Pelayanan.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPemeriksaanLyn'
})
export class DataFilterPemeriksaanLyn {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Component({
  selector: 'app-dokter',
  templateUrl: './list-pasien-igd.component.html',
  styleUrls: [
    './list-pasien-igd.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ListPasienIGDComponent implements OnInit {

  public useraccessdata: any;
  public data: any;

  // List Pasien IGD
  public dataPasienIgd: any;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "desc";
  public loading = false;

  // Resep Non Racik
  dataServiceNonRacik: RemoteData; // utk non racik
  obatNonRacikStr: string = '';

  // Resep Racik
  dataServiceRacik: RemoteData; // utk racik
  dataServListRacikEdit: RemoteData;
  dataServListRacikSave: RemoteData; // (tidak terpakai)
  obatRacikStr: string = '';

  // Diagnosa
  dataDiagnosaUtama: RemoteData; // utk diagnosa
  strDiagUtama: string = '';

  dataServiceDiagnosa: RemoteData; // utk diagnosa
  strDiagnosa: string = '';

  dataDiagnosaDetailUtama: RemoteData; // utk diagnosa
  strDiagDetailUtama: string = '';

  dataServiceDiagnosaD: RemoteData; // utk diagnosa
  strDiagnosaD: string = '';

  // Alkes
  dataServiceAlkesObat: RemoteData; // utk Alkes Obat
  strAlkes: string = '';

  // Diagnosa
  // Riwayat Diagnosa
  public filterRiwayatDiagnosa: string = "";
  dataAllDiagnosa: any;
  // Riwayat Diagnosa Detail
  public filterRiwayatDiagnosaD: string = "";
  dataAllDiagnosaD: any;

  // Paket obat Non racik
  public filterPaketObatNonRacik: string = "";
  // Paket obat racik
  public filterPaketObatRacik: string = "";

  constructor(private service: AppService, public session: SessionService, private datePipe: DatePipe,
    private completerService: CompleterService) {

    setInterval(() => this.reloadPage(), 60000);

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
    console.log(this.useraccessdata);

    // Resep Non Racik
    this.dataServiceNonRacik = completerService.remote(
      null,
      "ListObat",
      "ListObat");
    this.dataServiceNonRacik.urlFormater(term => {
      return `api/ResepNonRacik/ObatResep/` + this.useraccessdata.kd_user + term;
    });
    this.dataServiceNonRacik;

    // Resep Racik
    this.dataServiceRacik = completerService.remote(
      null,
      "ListObat",
      "ListObat");
    this.dataServiceRacik.urlFormater(term => {
      return `api/ResepNonRacik/ObatResep/` + this.useraccessdata.kd_user + term;
    });
    this.dataServiceRacik;

    // Resep Racik List Edit
    this.dataServListRacikEdit = completerService.remote(
      null,
      "ListObat",
      "ListObat");
    this.dataServListRacikEdit.urlFormater(term => {
      return `api/ResepNonRacik/ObatResep/` + this.useraccessdata.kd_user + term;
    });
    this.dataServListRacikEdit;

    // List Resep Racik Simpan (tidak terpakai)
    this.dataServListRacikSave = completerService.remote(
      null,
      "ListObat",
      "ListObat");
    this.dataServListRacikSave.urlFormater(term => {
      return `api/ResepNonRacik/ObatResep/` + this.useraccessdata.kd_user + term;
    });
    this.dataServListRacikSave;

    // Alkes Obat
    this.dataServiceAlkesObat = completerService.remote(
      null,
      "Nama_Barang",
      "Nama_Barang");
    this.dataServiceAlkesObat.urlFormater(term => {
      return `api/AlkesIgd/GetAlkesObat/` + term;
    });
    this.dataServiceAlkesObat;

    // Insert Diagnosa Pasien
    this.dataDiagnosaUtama = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaUtama.urlFormater(term => {
      return `api/Diagnosa/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaUtama;

    this.dataServiceDiagnosa = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataServiceDiagnosa.urlFormater(term => {
      return `api/Diagnosa/GetDiagnosaPasien/` + term;
    });
    this.dataServiceDiagnosa;

    // Edit Diagnosa Pasien
    this.dataDiagnosaDetailUtama = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaDetailUtama.urlFormater(term => {
      return `api/Diagnosa/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaDetailUtama;

    this.dataServiceDiagnosaD = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataServiceDiagnosaD.urlFormater(term => {
      return `api/Diagnosa/GetDiagnosaPasien/` + term;
    });
    this.dataServiceDiagnosaD;

  }

  reloadPage() {
    this.ngOnInit();
  }

  ngOnInit() {
    this.getPasienIGD();
  }

  getPasienIGD() {
    var paramListPasienIGD = {
      // tanggal: this.useraccessdata.tgl_hari_ini,
      // tanggal: '2020-02-10',
      tanggal: '2020-03-09',
      kd_dokter: this.useraccessdata.kd_user,
    }
    this.service.post("api/AppDokterIgd/ListPasienIGD", JSON.stringify(paramListPasienIGD))
      .subscribe(result => {
        if (result == "") {
          this.dataPasienIgd = "";
        } else {
          this.dataPasienIgd = JSON.parse(result);
        }
      },
        error => {
          window.alert("Mohon maaf telah terjadi kesalahan pada server");
        });
  }

  // Get Nama pasien
  nama_pasien: string = '';
  getNamaPasien(nama) {
    this.nama_pasien = nama;
  }

  // Get Kd RM dan RD No Reg
  kd_rm: string = '';
  getKdRMPasien(kdrm) {
    this.kd_rm = kdrm;
  }

  rd_noreg: string = '';
  getRdNoReg(rdnoreg) {
    this.rd_noreg = rdnoreg;
  }
  // Get Kd RM dan RD No Reg

  // get no igd
  public nomorIgd: any;
  public nomorIgd_baru: any;
  getNoIgd() {
    this.service.get("api/AppDokterIgd/GetNoIgd", '')
      .subscribe(result => {
        this.nomorIgd = JSON.parse(result).No_SO;
        console.log("No SO awal: ", this.nomorIgd);
      });
  }

  // Get Detail Pasien
  public dataDetailPasien: any;
  getPasienByRM() {
    var data: any;
    this.service.get("api/AppDokterIgd/GetDetailPasienIGD/" + this.kd_rm + "/" + this.rd_noreg, data)
      .subscribe(result => {
        this.dataDetailPasien = JSON.parse(result);
        this.convertTglLahir();
      });
  }

  tglLahir_pasien: string;
  nilaiUmur: string;
  convertTglLahir(): void {
    // format tgl
    this.tglLahir_pasien = this.datePipe.transform(this.dataDetailPasien.Tgl_Lahir, "dd-MM-yyyy");

    // convert ke umur
    let {
      AgeFromDateString,
      AgeFromDate
    } = require('age-calculator');
    let ageFromString = new AgeFromDateString(this.datePipe.transform(this.dataDetailPasien.Tgl_Lahir, "yyyy-MM-dd")).age;
    this.nilaiUmur = ageFromString;
  }
  // Get Detail Pasien

  cara_bayar: string = '';
  getCaraBayarPasien(cb) {
    this.cara_bayar = cb;
  }

  // Status Jasa Dokter
  statusFOC: string = '';
  checkStatusFOC(event: any) {
    this.statusFOC = event;
  }

  // Update Status Panggil
  ticket_id: string = '';
  getStatusPanggil(ticketid) {
    this.ticket_id = ticketid;
  }

  // Alergi
  alergi: string = '';
  getAlergi(item) {
    this.alergi = item;
    console.log(this.alergi);
  }

  // start resep
  // kondisi paket obat 
  public paketObatNORacik = true;
  public paketObatRacik = false;
  public paketAlkes = false;
  public paketPM = false;
  public paketTind = false;
  public hisLab = false;
  onTabChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab-resep-nonracik') {
      this.paketObatNORacik = true;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.hisLab = false;
    } else if ($event.nextId === 'tab-resep-racik') {
      this.paketObatNORacik = false;
      this.paketObatRacik = true;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.hisLab = false;
    } else if ($event.nextId === 'tab-alkes') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = true;
      this.paketPM = false;
      this.paketTind = false;
      this.hisLab = false;
    } else if ($event.nextId === 'tab-pm') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = true;
      this.paketTind = false;
      this.hisLab = false;
    } else if ($event.nextId === 'tab-tind') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = true;
      this.hisLab = false;
    } else if ($event.nextId === 'tab-lab') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.hisLab = true;
    }
  }

  cek_noso: string = '';
  cek_noso_racik: string = '';
  noso_edit: string = '';
  func_cekNoSOByNoReg() {
    this.service.get("api/ResepNonRacik/CekNoSOByNoReg/" + this.rd_noreg, '')
      .subscribe(result => {
        this.cek_noso = JSON.parse(result).Cek;
        this.noso_edit = JSON.parse(result).No_SO;
      },
        error => {
          this.loading = false;
        });
  }

  // on select obat non racik
  public arr_dataObatNonRacik = [];
  onSelectObatNonRacik(value) {
    let data = value["originalObject"];
    this.arr_dataObatNonRacik.push(data);
    this.obatNonRacikStr = '';

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN OBAT NON RACIK ( ' + data.Nm_Brg + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // Aturan Pakai, Jumlah, Delete Field Obat Non Racik
  aturanPakaiObatNonRacik(value, kd) {
    var index = this.arr_dataObatNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataObatNonRacik[index].Aturan_P = value;
    }
  }

  convertToRoman(num) {
    var roman = "";
    var values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var literals = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    for (var i = 0; i < values.length; i++) {
      if (num >= values[i]) {
        if (5 <= num && num <= 8) num -= 5;
        else if (1 <= num && num <= 3) num -= 1;
        else num -= values[i];
        roman += literals[i];
        i--;
      }
    }
    return roman;
  }

  jmlObatNonRacik(value, kd) {
    var index = this.arr_dataObatNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      value = parseInt(value);
      this.arr_dataObatNonRacik[index].Jml_SO = value;
      this.arr_dataObatNonRacik[index].Jml_RMW = this.convertToRoman(value);
    }
  }

  deleteFieldObatNonRacik(index) {
    this.arr_dataObatNonRacik.splice(index, 1);
  }
  // Aturan Pakai, Jumlah, Delete Field Obat Non Racik

  // Paket Obat Non Racik
  dataListPaketObatNonRacik: any;
  getPaketObatNonRacik() {
    var data: any;
    var kd_dokter = this.useraccessdata.kd_user;
    this.service.httpClientGet('api/ResepNonRacik/GetPaketObatByDok/' + kd_dokter, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketObatNonRacik = data;
      });
  };

  listPaketObatNonRacik: any;
  arr_dataPaketobatNonRacik = [];
  selectPaketObatNonRacik(item: any) {
    var kd_dokter = this.useraccessdata.kd_user;
    var kd_temp = item;
    this.service.httpClientGet('api/ResepNonRacik/GetObatByPaketObatNonRacik/' + kd_dokter + '/' + kd_temp, '')
      .subscribe(result => {
        if (result == "") {
          this.listPaketObatNonRacik = '';
        } else {
          this.listPaketObatNonRacik = result;
          this.arr_dataPaketobatNonRacik = this.listPaketObatNonRacik;
        }
      });
  }

  aturanPObatNonRacikPaket(value, kd) {
    var index = this.arr_dataPaketobatNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataPaketobatNonRacik[index].Aturan_P = value;
    }
  }

  jmlObatNonRacikPaket(value, kd) {
    var index = this.arr_dataPaketobatNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      value = parseInt(value);
      this.arr_dataPaketobatNonRacik[index].Jml_SO = value;
      this.arr_dataPaketobatNonRacik[index].Jml_RMW = this.convertToRoman(value);
    }
  }

  deleteFieldObatNonRacikPaket(index) {
    this.arr_dataPaketobatNonRacik.splice(index, 1);
  }
  // Paket Obat Non Racik

  // Pemberian Nama paket non racik baru
  dataObatPaketNonRacik: any;
  dataObatPaketNonRacikDetail: any;
  namaPaket: string;
  onSubmitPaketObatNonRacik() {
    this.loading = true;
    var paramPaketObatNonRacik = {
      kd_dokter: this.useraccessdata.kd_user,
      nm_template: this.namaPaket
    }
    // resep obat non racik
    for (var i = 0; i < this.arr_dataPaketobatNonRacik.length; i++) {
      this.arr_dataObatNonRacik.push(this.arr_dataPaketobatNonRacik[i]);
    }
    if (this.namaPaket != null && this.arr_dataObatNonRacik.length != 0) {
      this.service.post('api/ResepNonRacik/PostApSOTemplate_Resep', JSON.stringify(paramPaketObatNonRacik))
        .subscribe(result => {

          const paramLog = {
            NoReg: this.rd_noreg,
            Kd_Dokter: this.useraccessdata.kd_user,
            Aksi: 'MEMBUAT PAKET OBAT NON RACIK',
            Jenis: '2'
          }

          this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
            .subscribe(result => {
              this.loading = false;
            },
              err => {
                this.loading = false;
              });

          this.dataObatPaketNonRacik = JSON.parse(result);

          var paramPaketObatNonRacikDetail = {
            kd_dokter: this.useraccessdata.kd_user,
            data: this.arr_dataObatNonRacik
          }
          this.service.post('api/ResepNonRacik/PostApSOTemplate_Resep_dt', JSON.stringify(paramPaketObatNonRacikDetail))
            .subscribe(result => {
              this.dataObatPaketNonRacikDetail = JSON.parse(result);
              this.arr_dataObatNonRacik = [];
              this.arr_dataPaketobatNonRacik = [];
              this.namaPaket = '';
              this.getPaketObatNonRacik();
              this.loading = false;
            });
        },
          error => {
            this.namaPaket = '';
            this.arr_dataObatNonRacik = [];
            this.arr_dataPaketobatNonRacik = [];
            this.loading = false;
          });
    } else if (this.namaPaket == null) {
      this.loading = false;
      this.namaPaket = '';
    } else if (this.arr_dataObatNonRacik.length == 0) {
      this.loading = false;
      this.namaPaket = '';
    }
  }

  onClose() {
    this.dataObatNonRacik = [];
    this.dataResepRacik = [];
    this.arr_dataObatNonRacik = [];
    this.arr_dataPaketobatNonRacik = [];
    this.arr_dataObatRacik = [];
    this.arr_dataPaketObatRacik = [];
    this.catatanRacik = '';
    this.ketRacik = '';
    this.jmlNs = '';
    this.satuanRacik = '';
    this.dosisRacik = '';
    this.infoObatRacik = '';
    this.dataHLabDetail = [];
  }

  dataPostApSOHeader: any;
  dataPostApSONRacik: any;
  onSubmit() {
    this.loading = true;
    var ticket_posisiInt;
    // kondisi apabila resep obat non racik dan racik tidak terisi
    if (this.arr_dataObatNonRacik.length == 0 && this.arr_dataPaketobatNonRacik.length == 0 && this.arr_dataObatRacik.length == 0 && this.arr_dataPaketObatRacik.length == 0) {

      if (this.dataCekApSORacikH == undefined) { // kondisi apabila tidak ada resep
        if (this.cara_bayar == '1. Tunai' || this.cara_bayar == '2. Kartu Kredit' || this.cara_bayar == '3. Kartu Debet' || this.cara_bayar == '5. Keringanan' || this.cara_bayar == '6. FOC') {
          ticket_posisiInt = '3';
          var paramUpdSttsPanggil = {
            ticket_id: this.ticket_id,
            ticket_posisi: ticket_posisiInt
          }
          this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
            .subscribe(result => { });
        } else {
          ticket_posisiInt = '4';
          var paramUpdSttsPanggil = {
            ticket_id: this.ticket_id,
            ticket_posisi: ticket_posisiInt
          }
          this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
            .subscribe(result => { });
        }

        // Update alergi di table dbPasienRS jika tidak ada resep
        var paramAlergi = {
          alergi: this.alergi
        }
        this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
          .subscribe(result => { });
      }

    } else {
      this.service.get("api/ResepNonRacik/CekNoSO/" + this.nomorIgd.replace(/\//g, "@"), '')
        .subscribe(result => {
          this.cek_noso = JSON.parse(result).CekNoSO;

          if (this.cek_noso == "0") { // kondisi noso belum ada
            // kondisi jika resep obat non racik terisi
            ticket_posisiInt = '5';
            var paramUpdSttsPanggil = {
              ticket_id: this.ticket_id,
              ticket_posisi: ticket_posisiInt
            }
            this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
              .subscribe(result => { });

            // Post ApSOHeader
            var paramApSOHeader = {
              No_SO: this.nomorIgd,
              No_Reg: this.rd_noreg
            }
            // console.log('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader));
            this.service.post('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader))
              .subscribe(result => {
                this.dataPostApSOHeader = JSON.parse(result);

                // Post ApSONRacik
                for (var i = 0; i < this.arr_dataPaketobatNonRacik.length; i++) {
                  this.arr_dataObatNonRacik.push(this.arr_dataPaketobatNonRacik[i]);
                }
                if (this.arr_dataObatNonRacik.length != 0) { // kondisi apabila obat non racik tidak 0
                  // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
                  var paramApSONRacik = {
                    No_SO: this.nomorIgd,
                    data: this.arr_dataObatNonRacik
                  }
                  // console.log('(1) api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik));
                  this.service.post('api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik))
                    .subscribe(result => {
                      this.dataPostApSONRacik = JSON.parse(result);
                    });
                } else { // kondisi apabila obat non racik 0
                  // console.log('(1)');
                  this.loading = false;
                }

                // Update Sts SO dan Cetak
                this.service.put('api/ResepNonRacik/UpdateApSOHeader/' + this.nomorIgd.replace(/\//g, "@"), '')
                  .subscribe(result => { });

                var paramAlergi = {
                  alergi: this.alergi
                }
                this.service.put('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi))
                  .subscribe(result => { });

                this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
                  .subscribe(result => { });

                // reset masukan data
                this.arr_dataObatNonRacik = [];
                this.arr_dataPaketobatNonRacik = [];
                this.dataEditNamaRacik = [];
                this.dataEditGenerate = '';
                this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
                this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
              },
                error => {
                  this.loading = false;
                });
          }

          // pengkondisian apabila cek noso = 1 (ada)
          else if (this.cek_noso == "1") { // kondisi noso sudah ada
            this.service.get("api/ResepNonRacik/CekNoSOByNoReg/" + this.rd_noreg, '') // api utk cek noso berdasarkan noreg pasien
              .subscribe(result => {
                this.cek_noso = JSON.parse(result).Cek;
                this.nomorIgd = JSON.parse(result).No_SO;

                if (this.cek_noso == "1") { // kondisi apabila noso sudh ada di apshoheader berdasarkan noreg pasien
                  // looping obat non racik
                  for (var i = 0; i < this.arr_dataPaketobatNonRacik.length; i++) {
                    this.arr_dataObatNonRacik.push(this.arr_dataPaketobatNonRacik[i]);
                  }

                  if (this.arr_dataObatNonRacik.length != 0) { // kondisi apabila obat non racik tidak 0
                    // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
                    var paramApSONRacik = {
                      No_SO: this.nomorIgd,
                      data: this.arr_dataObatNonRacik
                    }
                    // console.log('(2) api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik));
                    this.service.post('api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik))
                      .subscribe(result => {
                        this.dataPostApSONRacik = JSON.parse(result);
                        this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
                        this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input

                        setTimeout(() => {
                          // redirect
                          this.arr_dataObatNonRacik = [];
                          this.arr_dataPaketobatNonRacik = [];
                          this.loading = false;
                          this.getPasienIGD();
                        }, 1000)
                      });
                  } else { // kondisi apabila obat non racik 0
                    this.loading = false;
                    // console.log('(2)');
                  }
                } else if (this.cek_noso == "0") { // kondisi apabila noso tidk ada di apshoheader berdasarkan noreg pasien
                  this.service.get("api/AppDokterIgd/GetNoIgd", '') // api generate noso baru
                    .subscribe(result => {
                      this.nomorIgd_baru = JSON.parse(result).No_SO;

                      // kondisi jika resep obat non racik terisi
                      ticket_posisiInt = '5';
                      var paramUpdSttsPanggil = {
                        ticket_id: this.ticket_id,
                        ticket_posisi: ticket_posisiInt
                      }
                      this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
                        .subscribe(result => { });

                      // Post ApSOHeader
                      var paramApSOHeader = {
                        No_SO: this.nomorIgd_baru,
                        No_Reg: this.rd_noreg
                      }
                      // console.log('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader));
                      this.service.post('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader))
                        .subscribe(result => {
                          this.dataPostApSOHeader = JSON.parse(result);

                          // Post ApSONRacik - looping obat non racik
                          for (var i = 0; i < this.arr_dataPaketobatNonRacik.length; i++) {
                            this.arr_dataObatNonRacik.push(this.arr_dataPaketobatNonRacik[i]);
                          }
                          if (this.arr_dataObatNonRacik.length != 0) {
                            // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
                            var paramApSONRacik = {
                              No_SO: this.nomorIgd_baru,
                              data: this.arr_dataObatNonRacik
                            }
                            // console.log('(3) api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik));
                            this.service.post('api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik))
                              .subscribe(result => {
                                this.dataPostApSONRacik = JSON.parse(result);
                                this.loading = false;
                              });
                          }
                          else {
                            this.loading = false;
                          }

                          // Update Sts SO dan Cetak
                          this.service.put('api/ResepNonRacik/UpdateApSOHeader/' + this.nomorIgd_baru.replace(/\//g, "@"), '')
                            .subscribe(result => { });

                          var paramAlergi = {
                            alergi: this.alergi
                          }
                          this.service.put('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi))
                            .subscribe(result => { });

                          this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
                            .subscribe(result => { });

                          // reset masukan data
                          this.arr_dataObatNonRacik = [];
                          this.arr_dataPaketobatNonRacik = [];
                          this.dataEditNamaRacik = [];
                          this.dataEditGenerate = '';
                          this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
                          this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
                        },
                          error => {
                            this.loading = false;
                          });
                    }); // generate noso baru
                }
              });
          }
        });
    }

    setTimeout(() => {
      this.getPasienIGD();
      this.dataEditNamaRacik = [];
      this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
      this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
      this.loading = false;
    }, 1000)
  }

  onUpdate() {
    this.loading = true;
    // api ce noso berdasarkan noreg pasien
    this.service.get("api/ResepNonRacik/CekNoSOByNoReg/" + this.rd_noreg, '')
      .subscribe(result => {
        this.cek_noso_racik = JSON.parse(result).Cek;
        this.noso_edit = JSON.parse(result).No_SO;

        for (var i = 0; i < this.arr_dataPaketobatNonRacik.length; i++) {
          this.arr_dataObatNonRacik.push(this.arr_dataPaketobatNonRacik[i]);
        }

        if (this.arr_dataObatNonRacik.length != 0) { // kondisi apabila obat non racik tidak 0
          // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
          var paramApSONRacik = {
            No_SO: this.noso_edit,
            data: this.arr_dataObatNonRacik
          }
          // console.log('(4) api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik));
          this.service.post('api/ResepNonRacik/PostApSONRacik', JSON.stringify(paramApSONRacik))
            .subscribe(result => {
              this.dataPostApSONRacik = JSON.parse(result);
              this.arr_dataObatNonRacik = [];
              this.arr_dataPaketobatNonRacik = [];
              this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
              this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
              this.loading = false;

              // // Update Sts SO = X dan Cetak
              this.service.put('api/ResepNonRacik/UpdateUlangApSOHeader/' + this.noso_edit.replace(/\//g, "@"), '')
                .subscribe(result => { });
            });
        } else {
          this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
          this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
          this.loading = false;
          // console.log('(4)');
        }

        var paramAlergi = {
          alergi: this.alergi
        }
        // console.log('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi));
        this.service.put('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi))
          .subscribe(result => { });

        // console.log('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi));
        this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
          .subscribe(result => { });

        setTimeout(() => {
          this.getPasienIGD();
          this.getResepObatNonRacik(); // fungsi utk menampilkan data obat non racik yg sudah di input
          this.func_getResepRacik(); // fungsi utk menampilkan data racik yg sudah di input
          this.loading = false;
        }, 1000);
      });
  }

  dataObatNonRacik: any;
  getResepObatNonRacik() {
    // this.loading = true;
    this.service.httpClientGet('api/ResepNonRacik/GetResepObatNonRacik/' + this.rd_noreg, '')
      .subscribe(result => {
        this.dataObatNonRacik = result;
        // console.log("Data Non Racik: ", this.dataObatNonRacik);
        // this.loading = false;
      });
  }

  deleteObatNonRacik(no_urut) {
    this.loading = true;
    var paramDelObatNonRacik = {
      no_so: this.noso_edit,
      no_urut: no_urut
    }
    this.service.post('api/ResepNonRacik/DeleteObatNonRacik', JSON.stringify(paramDelObatNonRacik))
      .subscribe(result => {
        this.dataEditNamaRacik = [];
        this.getResepObatNonRacik();
        this.func_getResepRacik();
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE OBAT NON RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  // Start Racik
  arr_dataObatRacik = [];
  arr_dataPaketObatRacik = [];
  dataListPaketObatRacik: any;
  listObatPaketR: any;
  /** Paket Obat Racik */
  getPaketObatRacik() {
    var data: any;
    var kd_dokter = this.useraccessdata.kd_user;
    this.service.httpClientGet('api/ResepRacik/GetPaketObatRacikByDok/' + kd_dokter, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketObatRacik = data;
      });
  }

  SelectPaketRacik(item: any) {
    var kd_dokter = this.useraccessdata.kd_user;
    var kd_paket = item;
    this.service.httpClientGet('api/ResepRacik/GetObatByPaketObatRacik/' + kd_dokter + '/' + kd_paket, '')
      .subscribe(result => {
        if (result == "") {
          this.listObatPaketR = '';
        } else {
          this.listObatPaketR = result;
          this.arr_dataPaketObatRacik = this.listObatPaketR;
        }
      });
  }

  listSatuanObat: any;
  getSatuanObat() {
    this.service.get("api/ResepRacik/GetSatuanObat", '')
      .subscribe(result => {
        this.listSatuanObat = JSON.parse(result);
      });
  }

  onSelectObatRacik(value) {
    let data = value["originalObject"];
    this.arr_dataObatRacik.push(data);
    this.obatRacikStr = '';
    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN OBAT RACIK ( ' + data.Nm_Brg + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // obat racik saja
  editJmlObatRacik(value, kd) {
    var index = this.arr_dataObatRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataObatRacik[index].Jml_SO = value;
    }
  }

  selSatuanRacikObat(value, kd) {
    console.log(value, kd);
    var index = this.arr_dataObatRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataObatRacik[index].Sat_SO = value;
    }
  }

  deleteFieldObatRacik(index) {
    this.arr_dataObatRacik.splice(index, 1);
  }

  // paket obat racik
  editJmlPaketObatRacik(value, kd) {
    var index = this.arr_dataPaketObatRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataPaketObatRacik[index].Jml_SO = value;
    }
  }

  selSatPaketObatRacik(value, kd) {
    var index = this.arr_dataPaketObatRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.arr_dataPaketObatRacik[index].Sat_SO = value;
    }
  }

  deleteFieldPaketObatRacik(index) {
    this.arr_dataPaketObatRacik.splice(index, 1);
  }

  // Paket Obat Racik
  kdTempalate = '';
  namaPaketRacik = '';
  satuan = '';
  aturanPakai = '';
  dataPaketObatRacikNew: any;
  dataObatRacikNew: any;
  onSubmitPaketObatRacik() {
    this.loading = true;
    var inputParamInsertPaketRacik = {
      kd_dokter: this.useraccessdata.kd_user,
      nm_template: this.namaPaketRacik,
      satuan: this.satuan,
      aturan_pakai: this.aturanPakai
    }

    for (var i = 0; i < this.arr_dataPaketObatRacik.length; i++) {
      this.arr_dataObatRacik.push(this.arr_dataPaketObatRacik[i]);
    }

    this.service.post('api/ResepRacik/PostApSOTemplate_Resep_Racik', JSON.stringify(inputParamInsertPaketRacik))
      .subscribe(result => {
        this.dataPaketObatRacikNew = JSON.parse(result);
        this.kdTempalate = this.dataPaketObatRacikNew.Kd_Template;

        var inputParamInsertPaketObatRacik = {
          kd_dokter: this.useraccessdata.kd_user,
          kd_template: this.kdTempalate,
          data: this.arr_dataObatRacik
        }

        this.service.post('api/ResepRacik/PostApSOTemplate_Resep_Racik_dt', JSON.stringify(inputParamInsertPaketObatRacik))
          .subscribe(result => {
            this.dataObatRacikNew = JSON.parse(result);
            this.arr_dataObatRacik = [];
            this.arr_dataPaketObatRacik = [];

            this.getPaketObatRacik();
            this.loading = false;
          });

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MEMBUAT PAKET OBAT RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      },
        error => {
          // this.service.errorserver();
          this.namaPaketRacik = '';
          this.satuan = '';
          this.aturanPakai = '';
          this.arr_dataObatRacik = [];
          this.arr_dataPaketObatRacik = [];
          this.loading = false;
        });
  }

  // Racik
  dataGenerate: any;
  kdRacik: string = '';
  noUrutRacik: string = '';
  namaRacik: string = '';
  getGeneRacik() {
    var paramGene = {
      no_so: this.nomorIgd
    }
    this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
      .subscribe(result => {
        this.dataGenerate = JSON.parse(result);
        this.kdRacik = this.dataGenerate.kd_Racik;
        this.noUrutRacik = this.dataGenerate.NoUrut;
        this.namaRacik = this.dataGenerate.Nama_Racikan;
      });
  }

  dataEditGenerate: any;
  edit_kdRacik: string = '';
  edit_noUrutRacik: string = '';
  edit_namaRacik: string = '';
  getGeneEditRacik() {
    // Resep Racik
    // Generate Racik Header
    var paramGene = {
      no_so: this.noso_edit
    }
    this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
      .subscribe(result => {
        this.dataEditGenerate = JSON.parse(result);
        this.edit_kdRacik = this.dataEditGenerate.kd_Racik;
        this.edit_noUrutRacik = this.dataEditGenerate.NoUrut;
        this.edit_namaRacik = this.dataEditGenerate.Nama_Racikan;
      });
  }

  // perdosis
  valDoSel = '';
  dosis_seluruhChange(value) {
    this.valDoSel = value;
    if (this.valDoSel == 'Perdosis') {
      this.catatanRacik = 'Racik dibuat perdosis';
    } else if (this.valDoSel == 'Keseluruhan') {
      this.catatanRacik = 'Racik dibuat keseluruhan';
    }
  }

  dataCekApSORacikH: any;
  no_soCekRacikH: string = '';
  func_cekNoSOApSORacikH() {
    var data: any;
    this.service.get("api/ResepRacik/CekNoSOApSORacikH/" + this.nomorIgd.replace(/\//g, "@"), data)
      .subscribe(result => {
        this.dataCekApSORacikH = JSON.parse(result).CekRacikH;
      });
  }

  // fungsi menampilkan data resep racik yg sudah di tersimpan di db
  dataResepRacik: any;
  func_getResepRacik() {
    this.loading = true;
    this.service.httpClientGet('api/ResepRacik/GetResepObatRacik/' + this.rd_noreg, '')
      .subscribe(result => {
        this.dataResepRacik = result;
        console.log("Data Racik: ", this.dataResepRacik);
        this.loading = false;
      });
  }

  detailListObatRacik: any;
  kdRacikEditList = '';
  noUrutRacikEditList = '';
  namaRacikEditList = '';
  jmlRacikEditList = '';
  satRacikEditList = '';
  aturanRacikEditList = '';
  func_GetDetailResepObatRacik(noso, kdRacik, nourutRacik, nmRacik, jmlRacik, satRacik, aturanRacik) {
    this.loading = true;
    this.noso_edit = noso;
    this.kdRacikEditList = kdRacik;
    this.noUrutRacikEditList = nourutRacik;
    this.namaRacikEditList = nmRacik;
    this.jmlRacikEditList = jmlRacik;
    this.satRacikEditList = satRacik;
    this.aturanRacikEditList = aturanRacik;
    console.log("1.", 'api/ResepRacik/GetResepObatRacikDetail/' + this.noso_edit.replace(/\//g, "@") + '/' + this.kdRacikEditList, '');
    this.service.httpClientGet('api/ResepRacik/GetResepObatRacikDetail/' + this.noso_edit.replace(/\//g, "@") + '/' + this.kdRacikEditList, '')
      .subscribe(result => {
        this.detailListObatRacik = result;
        this.loading = false;
      });
  }

  NewdetailListObatRacik: any;
  NewkdRacikEditList = '';
  NewnoUrutRacikEditList = '';
  NewnamaRacikEditList = '';
  NewjmlRacikEditList = '';
  NewsatRacikEditList = '';
  NewaturanRacikEditList = '';
  Newfunc_GetDetailResepObatRacik(kdRacik, nourutRacik, nmRacik, jmlRacik, satRacik, aturanRacik) {
    this.loading = true;
    this.NewkdRacikEditList = kdRacik;
    this.NewnoUrutRacikEditList = nourutRacik;
    this.NewnamaRacikEditList = nmRacik;
    this.NewjmlRacikEditList = jmlRacik;
    this.NewsatRacikEditList = satRacik;
    this.NewaturanRacikEditList = aturanRacik;
    console.log("2.", 'api/ResepRacik/GetResepObatRacikDetail/' + this.nomorIgd.replace(/\//g, "@") + '/' + this.NewkdRacikEditList, '')
    this.service.httpClientGet('api/ResepRacik/GetResepObatRacikDetail/' + this.nomorIgd.replace(/\//g, "@") + '/' + this.NewkdRacikEditList, '')
      .subscribe(result => {
        this.NewdetailListObatRacik = result;
        this.loading = false;
      });
  }


  // menampilkan resep racik baru di form simpan / blm punya so header
  dataEditNamaRacik: any;
  getResepRacikNew() {
    this.service.httpClientGet('api/ResepRacik/GetResepRacikNew/' + this.nomorIgd.replace(/\//g, "@"), '')
      .subscribe(result => {
        this.dataEditNamaRacik = result;
      });
  }

  // yg lama
  catatanRacik = '';
  ketRacik = '';
  jmlNs = '';
  satuanRacik = '';
  dosisRacik = '';
  infoObatRacik = '';
  dataApSoRacikH: any;
  dataApSORacikD: any;
  onSubmitRacikan() {

    this.loading = true;
    this.service.get("api/ResepNonRacik/CekNoSO/" + this.nomorIgd.replace(/\//g, "@"), '')
      .subscribe(result => {
        this.cek_noso = JSON.parse(result).CekNoSO;

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'INPUT RESEP OBAT RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        // pengecekan awal noso
        // jika noso tidak ada = 0
        // dibuat noso header racik header racik detail
        // kondisi apabila ce noso di apsoheader tidak ada berdasarkan noso yg diambil di awal
        // jadi menggunakan noso yg diambil diawal
        if (this.cek_noso == "0") {
          var ticket_posisiInt;
          // kondisi dimana apabila terdapat resep obat racik terisi maka tiket posisi = 5
          ticket_posisiInt = '5';
          var paramUpdSttsPanggil = {
            ticket_id: this.ticket_id,
            ticket_posisi: ticket_posisiInt
          }
          this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
            .subscribe(result => { });

          // Post ApSOHeader
          var paramApSOHeader = {
            No_SO: this.nomorIgd,
            No_Reg: this.rd_noreg
          }
          console.log('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader));
          this.service.post('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader))
            .subscribe(result => {
              this.dataPostApSOHeader = JSON.parse(result);
              this.service.put('api/ResepNonRacik/UpdateApSOHeader/' + this.nomorIgd.replace(/\//g, "@"), '')
                .subscribe(result => { });

              // api generate kode racik berdasarkan noso yg diambil diawal
              var paramGene = {
                no_so: this.nomorIgd
              }
              this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                .subscribe(result => {
                  this.dataGenerate = JSON.parse(result);
                  this.kdRacik = this.dataGenerate.kd_Racik;
                  this.noUrutRacik = this.dataGenerate.NoUrut;
                  this.namaRacik = this.dataGenerate.Nama_Racikan;

                  // looping data obat racik
                  for (var i = 0; i < this.arr_dataPaketObatRacik.length; i++) {
                    this.arr_dataObatRacik.push(this.arr_dataPaketObatRacik[i]);
                  }

                  // Post ApSORacikH
                  var paramApSoRacikH = {
                    no_so: this.nomorIgd,
                    kd_racik: this.kdRacik,
                    no_urut: this.noUrutRacik,
                    nama_racikan: this.namaRacik,
                    jml_ns: this.jmlNs,
                    dosis_racik: this.dosisRacik,
                    satuan_racik: this.satuanRacik,
                    info_obat_racik: this.infoObatRacik,
                    catatan_racik: this.catatanRacik,
                    ket_racik: this.ketRacik,
                    jml_rmw: this.convertToRoman(this.jmlNs)
                  }
                  // pengkondisian apabila detail obat racik tidak = 0
                  if (this.arr_dataObatRacik.length != 0) {
                    this.service.post('api/ResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                      .subscribe(result => {
                        this.dataApSoRacikH = JSON.parse(result);

                        // Post ApSoRacikD
                        var paramApSORacikD = {
                          no_so: this.nomorIgd,
                          kd_racik: this.kdRacik,
                          no_urut: this.noUrutRacik,
                          dataRacik: this.arr_dataObatRacik
                        }
                        this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                          .subscribe(result => {
                            this.dataApSORacikD = JSON.parse(result);
                            this.loading = false;
                          });

                        // reset masukan resep racik
                        this.jmlNs = '';
                        this.dosisRacik = '';
                        this.satuanRacik = '';
                        this.infoObatRacik = '';
                        this.catatanRacik = '';
                        this.ketRacik = '';
                        this.arr_dataObatRacik = [];
                        this.arr_dataPaketObatRacik = [];
                        this.obatRacikStr = '';
                      },
                        error => {
                          this.loading = false;
                        });

                    setTimeout(() => {
                      // this.getGeneRacik();
                      this.func_cekNoSOApSORacikH();
                      this.getResepRacikNew();
                    }, 1000)
                  } else {
                    // window.alert("Resep racik obat tidak ada");
                    this.loading = false;
                  }
                });

              var paramAlergi = {
                alergi: this.alergi
              }
              this.service.put('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi))
                .subscribe(result => { });

              this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
                .subscribe(result => { });
            },
              error => {
                this.loading = false;
              });
        } else if (this.cek_noso == "1") { // pengkondisian apabila noso sudah ada = 1 berdasarkan noso yg diambil di awal
          // menjalankan api utk mengecek noso apa sudh tersimpan berdasarkan noreg
          this.service.get("api/ResepNonRacik/CekNoSOByNoReg/" + this.rd_noreg, '')
            .subscribe(result => {
              this.cek_noso_racik = JSON.parse(result).Cek;
              this.nomorIgd = JSON.parse(result).No_SO;

              // kondisi noso sudah ada berdasarkan noreg
              // lalu yg tersimpan hanya racik h dan d
              if (this.cek_noso_racik == "1") { // kondisi noso sudh ada di apsoheader berdasarkan noreg pasien
                var paramGene = {
                  no_so: this.nomorIgd
                }
                // generate kode racik berdasarkan noso yg sudh ada
                this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                  .subscribe(result => {
                    this.dataGenerate = JSON.parse(result);
                    this.kdRacik = this.dataGenerate.kd_Racik;
                    this.noUrutRacik = this.dataGenerate.NoUrut;
                    this.namaRacik = this.dataGenerate.Nama_Racikan;

                    // looping data obat racik
                    for (var i = 0; i < this.arr_dataPaketObatRacik.length; i++) {
                      this.arr_dataObatRacik.push(this.arr_dataPaketObatRacik[i]);
                    }

                    // Post ApSORacikD
                    var paramApSoRacikH = {
                      no_so: this.nomorIgd,
                      kd_racik: this.kdRacik,
                      no_urut: this.noUrutRacik,
                      nama_racikan: this.namaRacik,
                      jml_ns: this.jmlNs,
                      dosis_racik: this.dosisRacik,
                      satuan_racik: this.satuanRacik,
                      info_obat_racik: this.infoObatRacik,
                      catatan_racik: this.catatanRacik,
                      ket_racik: this.ketRacik,
                      jml_rmw: this.convertToRoman(this.jmlNs)
                    }

                    if (this.arr_dataObatRacik.length != 0) { // cek obat racik tidak kosong
                      this.service.post('api/ResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                        .subscribe(result => {
                          this.dataApSoRacikH = JSON.parse(result);

                          // Post ApSoRacikD
                          var paramApSORacikD = {
                            no_so: this.nomorIgd,
                            kd_racik: this.kdRacik,
                            no_urut: this.noUrutRacik,
                            dataRacik: this.arr_dataObatRacik
                          }
                          this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                            .subscribe(result => {
                              this.dataApSORacikD = JSON.parse(result);
                            });

                          // reset masukan resep racik
                          this.jmlNs = '';
                          this.dosisRacik = '';
                          this.satuanRacik = '';
                          this.infoObatRacik = '';
                          this.catatanRacik = '';
                          this.ketRacik = '';
                          this.arr_dataObatRacik = [];
                          this.arr_dataPaketObatRacik = [];
                          this.obatRacikStr = '';
                        },
                          error => {
                            this.loading = false;
                          });

                      setTimeout(() => {
                        // menampilkan data racik yg sudah disimpan
                        this.service.httpClientGet('api/ResepRacik/GetResepRacikNew/' + this.nomorIgd.replace(/\//g, "@"), '')
                          .subscribe(result => {
                            this.dataEditNamaRacik = result;
                          });
                        this.loading = false;
                      }, 1000)
                    } else { // cek obat racik kosong
                      // window.alert("Resep racik obat tidak ada");
                      this.loading = false;
                    }
                  });
              }
              // kondisi noso tidak ada = 0 berdasarkan noreg
              // makan dibuat baru noso nya
              else if (this.cek_noso_racik == "0") {
                this.service.get("api/AppDokterIgd/GetNoIgd", '') // api generate noso baru
                  .subscribe(result => {
                    this.nomorIgd_baru = JSON.parse(result).No_SO;

                    var ticket_posisiInt;
                    // kondisi dimana apabila terdapat resep obat racik terisi maka tiket posisi = 5
                    ticket_posisiInt = '5';
                    var paramUpdSttsPanggil = {
                      ticket_id: this.ticket_id,
                      ticket_posisi: ticket_posisiInt
                    }
                    this.service.post('api/AppDokterIgd/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
                      .subscribe(result => { });

                    // Post ApSOHeader
                    var paramApSOHeader = {
                      No_SO: this.nomorIgd_baru,
                      No_Reg: this.rd_noreg
                    }
                    console.log('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader));
                    this.service.post('api/ResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOHeader))
                      .subscribe(result => {
                        this.dataPostApSOHeader = JSON.parse(result);
                        this.service.put('api/ResepNonRacik/UpdateApSOHeader/' + this.nomorIgd_baru.replace(/\//g, "@"), '')
                          .subscribe(result => { });

                        // pembuatan kode racik baru dng noso baru
                        var paramGene = {
                          no_so: this.nomorIgd_baru
                        }
                        this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                          .subscribe(result => {
                            this.dataGenerate = JSON.parse(result);
                            this.kdRacik = this.dataGenerate.kd_Racik;
                            this.noUrutRacik = this.dataGenerate.NoUrut;
                            this.namaRacik = this.dataGenerate.Nama_Racikan;

                            // looping data obat racik
                            for (var i = 0; i < this.arr_dataPaketObatRacik.length; i++) {
                              this.arr_dataObatRacik.push(this.arr_dataPaketObatRacik[i]);
                            }

                            // Post ApSORacikH
                            var paramApSoRacikH = {
                              no_so: this.nomorIgd_baru,
                              kd_racik: this.kdRacik,
                              no_urut: this.noUrutRacik,
                              nama_racikan: this.namaRacik,
                              jml_ns: this.jmlNs,
                              dosis_racik: this.dosisRacik,
                              satuan_racik: this.satuanRacik,
                              info_obat_racik: this.infoObatRacik,
                              catatan_racik: this.catatanRacik,
                              ket_racik: this.ketRacik,
                              jml_rmw: this.convertToRoman(this.jmlNs)
                            }
                            // pengkondisian apabila detail obat racik tidak = 0
                            if (this.arr_dataObatRacik.length != 0) {
                              this.service.post('api/ResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                                .subscribe(result => {
                                  this.dataApSoRacikH = JSON.parse(result);

                                  // Post ApSoRacikD
                                  var paramApSORacikD = {
                                    no_so: this.nomorIgd_baru,
                                    kd_racik: this.kdRacik,
                                    no_urut: this.noUrutRacik,
                                    dataRacik: this.arr_dataObatRacik
                                  }
                                  this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                                    .subscribe(result => {
                                      this.dataApSORacikD = JSON.parse(result);
                                      this.loading = false;
                                    });

                                  // reset masukan resep racik
                                  this.jmlNs = '';
                                  this.dosisRacik = '';
                                  this.satuanRacik = '';
                                  this.infoObatRacik = '';
                                  this.catatanRacik = '';
                                  this.ketRacik = '';
                                  this.arr_dataObatRacik = [];
                                  this.arr_dataPaketObatRacik = [];
                                  this.obatRacikStr = '';
                                },
                                  error => {
                                    this.loading = false;
                                  });

                              setTimeout(() => {
                                // this.getGeneRacik();
                                var paramGene = {
                                  no_so: this.nomorIgd_baru
                                }
                                this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                                  .subscribe(result => {
                                    this.dataGenerate = JSON.parse(result);
                                    this.kdRacik = this.dataGenerate.kd_Racik;
                                    this.noUrutRacik = this.dataGenerate.NoUrut;
                                    this.namaRacik = this.dataGenerate.Nama_Racikan;
                                  });

                                // this.func_cekNoSOApSORacikH();
                                var data: any;
                                this.service.get("api/ResepRacik/CekNoSOApSORacikH/" + this.nomorIgd_baru.replace(/\//g, "@"), data)
                                  .subscribe(result => {
                                    this.dataCekApSORacikH = JSON.parse(result).CekRacikH;
                                  });

                                // this.getResepRacikNew();
                                this.service.httpClientGet('api/ResepRacik/GetResepRacikNew/' + this.nomorIgd_baru.replace(/\//g, "@"), '')
                                  .subscribe(result => {
                                    this.dataEditNamaRacik = result;
                                  });
                              }, 1000)
                            } else {
                              // window.alert("Resep racik obat tidak ada");
                              this.loading = false;
                            }
                          });

                        var paramAlergi = {
                          alergi: this.alergi
                        }
                        this.service.put('api/AppDokterIgd/UpdateALergiApSOHeader/' + this.rd_noreg, JSON.stringify(paramAlergi))
                          .subscribe(result => { });

                        this.service.put('api/AppDokterIgd/UpdateALergidbPasienRS/' + this.kd_rm, JSON.stringify(paramAlergi))
                          .subscribe(result => { });
                      },
                        error => {
                          this.loading = false;
                        });
                  })
              }
            });
        }
      });
  }

  onUpdateRacikan() {
    this.loading = true;
    // api cek noso berdasarkan noreg
    this.service.get("api/ResepNonRacik/CekNoSOByNoReg/" + this.rd_noreg, '')
      .subscribe(result => {
        this.cek_noso_racik = JSON.parse(result).Cek;
        this.noso_edit = JSON.parse(result).No_SO;

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'INPUT RESEP OBAT RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        if (this.cek_noso_racik == "1") { // kondisi apabila noso sudah ada
          var paramGene = {
            no_so: this.noso_edit
          }
          // api generate kode racik berdasarkan noso yg sudah ada
          this.service.post('api/ResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
            .subscribe(result => {
              this.dataGenerate = JSON.parse(result);
              this.edit_kdRacik = this.dataGenerate.kd_Racik;
              this.edit_noUrutRacik = this.dataGenerate.NoUrut;
              this.edit_namaRacik = this.dataGenerate.Nama_Racikan;

              // looping data obat racik
              for (var i = 0; i < this.arr_dataPaketObatRacik.length; i++) {
                this.arr_dataObatRacik.push(this.arr_dataPaketObatRacik[i]);
              }

              // Post ApSORacikD
              var paramApSoRacikH = {
                no_so: this.noso_edit,
                kd_racik: this.edit_kdRacik,
                no_urut: this.edit_noUrutRacik,
                nama_racikan: this.edit_namaRacik,
                jml_ns: this.jmlNs,
                dosis_racik: this.dosisRacik,
                satuan_racik: this.satuanRacik,
                info_obat_racik: this.infoObatRacik,
                catatan_racik: this.catatanRacik,
                ket_racik: this.ketRacik,
                jml_rmw: this.convertToRoman(this.jmlNs)
              }

              if (this.arr_dataObatRacik.length != 0) { // cek obat racik tidak kosong
                this.service.post('api/ResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                  .subscribe(result => {
                    this.dataApSoRacikH = JSON.parse(result);

                    // Post ApSoRacikD
                    var paramApSORacikD = {
                      no_so: this.noso_edit,
                      kd_racik: this.edit_kdRacik,
                      no_urut: this.edit_noUrutRacik,
                      dataRacik: this.arr_dataObatRacik
                    }
                    this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                      .subscribe(result => {
                        this.dataApSORacikD = JSON.parse(result);
                      });

                    // Updace cetak ulang
                    // Update Sts SO = X dan Cetak
                    this.service.put('api/ResepNonRacik/UpdateUlangApSOHeader/' + this.noso_edit.replace(/\//g, "@"), '')
                      .subscribe(result => { });

                    // reset masukan resep racik
                    this.jmlNs = '';
                    this.dosisRacik = '';
                    this.satuanRacik = '';
                    this.infoObatRacik = '';
                    this.catatanRacik = '';
                    this.ketRacik = '';
                    this.dataEditNamaRacik = [];
                    this.arr_dataObatRacik = [];
                    this.arr_dataPaketObatRacik = [];
                    this.obatRacikStr = '';
                  },
                    error => {
                      this.loading = false;
                    });

                setTimeout(() => {
                  this.getGeneRacik();
                  this.func_cekNoSOApSORacikH();
                  this.func_getResepRacik();
                }, 1000)
              } else { // cek obat racik kosong
                // window.alert("Resep racik obat tidak ada");
                this.loading = false;
              }

            });
        }
      });
  }

  // nambah obat racik, sudh punya noso
  dataAddUpdObatListRacikan: any;
  onUpdateObatListRacikan() {
    this.loading = true;
    for (var i = 0; i < this.arr_dataObatRacik.length; i++) {
      this.arr_dataObatRacik[i];
    }
    var paramApSORacikD = {
      no_so: this.noso_edit,
      kd_racik: this.kdRacikEditList,
      no_urut: this.noUrutRacikEditList,
      dataRacik: this.arr_dataObatRacik
    }

    if (this.arr_dataObatRacik.length != 0) {
      this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
        .subscribe(result => {
          this.dataAddUpdObatListRacikan = JSON.parse(result);
          this.func_GetDetailResepObatRacik(this.noso_edit, this.kdRacikEditList, this.noUrutRacikEditList, this.namaRacikEditList,
            this.jmlRacikEditList, this.satRacikEditList, this.aturanRacikEditList)

          this.arr_dataObatRacik = [];
          this.obatRacikStr = '';
          this.loading = false;
        });
    } else {
      this.loading = false;
    }
  }

  // fungsi delete racik header
  deleteApSORacikHEdit(noso, kdracik) {
    this.loading = true;
    var paramDeleteApSORacikH = {
      no_so: noso,
      kd_racik: kdracik
    }
    console.log('api/ResepRacik/DeleteApSORacikH', JSON.stringify(paramDeleteApSORacikH));
    this.service.post('api/ResepRacik/DeleteApSORacikH', JSON.stringify(paramDeleteApSORacikH))
      .subscribe(result => {
        // this.getGeneRacik();
        this.dataEditNamaRacik = [];
        this.getResepObatNonRacik();
        this.func_getResepRacik();
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE RESEP OBAT RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  // fungsi delete racik detail yg sudah punya so (tidak terpakai)
  deleteApSORacikDEdit(noso, kdracik, kdbrg) {
    this.loading = true;
    var paramDeleteApSORacikD = {
      no_so: noso,
      kd_racik: kdracik,
      kd_brg: kdbrg
    }
    console.log('api/ResepRacik/DeleteApSORacikD', JSON.stringify(paramDeleteApSORacikD));
    this.service.post('api/ResepRacik/DeleteApSORacikD', JSON.stringify(paramDeleteApSORacikD))
      .subscribe(result => {
        this.func_GetDetailResepObatRacik(this.noso_edit, this.kdRacikEditList, this.noUrutRacikEditList, this.namaRacikEditList,
          this.jmlRacikEditList, this.satRacikEditList, this.aturanRacikEditList)
        this.loading = false;

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE OBAT RACIK',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  // nambah obat racik, blm punya noso (tidk terpakai)
  onSubmitObatListRacikan() {
    this.loading = true;
    for (var i = 0; i < this.arr_dataObatRacik.length; i++) {
      this.arr_dataObatRacik[i];
    }
    var paramApSORacikD = {
      no_so: this.nomorIgd,
      kd_racik: this.NewkdRacikEditList,
      no_urut: this.NewnoUrutRacikEditList,
      dataRacik: this.arr_dataObatRacik
    }
    if (this.arr_dataObatRacik.length != 0) {
      this.service.post('api/ResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
        .subscribe(result => {
          this.dataAddUpdObatListRacikan = JSON.parse(result);
          this.Newfunc_GetDetailResepObatRacik(this.NewkdRacikEditList, this.NewnoUrutRacikEditList, this.NewnamaRacikEditList, this.NewjmlRacikEditList, this.NewsatRacikEditList, this.NewaturanRacikEditList)
          this.arr_dataObatRacik = [];
          this.obatRacikStr = '';
          this.loading = false;
        });
    } else {
      // window.alert("Resep racik obat tidak ada");
      this.loading = false;
    }
  }

  // fungsi delete racik header yg belum punya no so
  deleteApSORacikH(kdracik) {
    this.loading = true;
    var paramDeleteApSORacikH = {
      no_so: this.nomorIgd,
      kd_racik: kdracik
    }
    this.service.post('api/ResepRacik/DeleteApSORacikH', JSON.stringify(paramDeleteApSORacikH))
      .subscribe(result => {
        this.getGeneRacik();
        this.getResepRacikNew();
        this.func_cekNoSOApSORacikH();
        this.loading = false;
      });
  }

  // fungsi delete racik detail yg belum punya so
  deleteApSORacikD(kdracik, kdbrg) {
    this.loading = true;
    var paramDeleteApSORacikD = {
      no_so: this.nomorIgd,
      kd_racik: kdracik,
      kd_brg: kdbrg
    }
    this.service.post('api/ResepRacik/DeleteApSORacikD', JSON.stringify(paramDeleteApSORacikD))
      .subscribe(result => {
        this.Newfunc_GetDetailResepObatRacik(this.NewkdRacikEditList, this.NewnoUrutRacikEditList,
          this.NewnamaRacikEditList, this.NewjmlRacikEditList, this.NewsatRacikEditList,
          this.NewaturanRacikEditList);
        this.loading = false;
      });
  }

  // ----------------------- START DIAGNOSA ------------------------
  // Riwayat Diagnosa
  getAllRiwayatDiagnosaPasien() {
    this.loading = true;
    this.service.get("api/Diagnosa/GetAllDiagnosaPasien/" + this.kd_rm, '')
      .subscribe(result => {
        this.dataAllDiagnosa = JSON.parse(result);
        this.loading = false;
      },
        error => {
          this.loading = false;
        });
  }

  // view riwayat diagnosa sebelah kanan
  dataDetailDiagnosa: any;
  subjDetailView: string = '';
  ObjDetailView: string = '';
  diagUtamaView: string = '';
  planDetailView: string = '';
  dataDetailResepNRacik: any;
  dataDetailResepRacik: any;
  dataDiagnosaDRiwayat: any;
  getDetailRiwayatDiagnosaPasien(noreg) {
    this.loading = true;

    // skrinning
    this.service.get("api/Diagnosa/GetSkrinningPasien/" + noreg, '')
      .subscribe(result => {
        this.subjDetailView = JSON.parse(result).KeluhanUtama;

        // pemeriksaan fisik & planning
        // api untuk mengambil riwayat detail pemeriksaan fisik dan planning
        this.service.get("api/Diagnosa/GetDetailDiagnosaPasien/" + this.kd_rm + "/" + noreg, '')
          .subscribe(result => {
            this.dataDetailDiagnosa = JSON.parse(result);
            this.ObjDetailView = this.dataDetailDiagnosa.PemeriksaanFisik;
            this.diagUtamaView = this.dataDetailDiagnosa.Ket_ICD;
            this.planDetailView = this.dataDetailDiagnosa.Planning;

            // detail diagnosa
            // baru tanggal 10-09-2019
            this.service.get('api/Diagnosa/GetDiagnosaDetailByNoReg/' + noreg, '')
              .subscribe(result => {
                this.dataDiagnosaDRiwayat = JSON.parse(result);
                this.loading = false;
              },
                error => {
                  this.loading = false;
                }); // detail diagnosa
            // baru tanggal 10-09-2019

          },
            error => {
              this.loading = false;
            }); // pemeriksaan fisik & planning

      },
        error => {
          this.loading = false;
        }); // skrinning   

    this.service.get("api/ResepNonRacik/GetAllRiwayatResepNRacik/" + noreg, '')
      .subscribe(result => {
        this.dataDetailResepNRacik = JSON.parse(result);
      },
        error => {
          this.loading = false;
        });

    this.service.get("api/ResepRacik/GetAllRiwayatResepRacik/" + noreg, '')
      .subscribe(result => {
        this.dataDetailResepRacik = JSON.parse(result);
      },
        error => {
          this.loading = false;
        });
  }

  resetRiwayatDetailDiag() {
    this.subjDetailView = '';
    this.ObjDetailView = '';
    this.diagUtamaView = '';
    this.dataDiagnosaDRiwayat = [];
    this.planDetailView = '';
    this.diagUtamaDetailPilih = '';
    this.dataAllDiagnosa = [];
    this.dataDetailResepNRacik = [];
    this.dataDetailResepRacik = [];
  }
  // Riwayat Diagnosa

  // input diagnosa baru
  keluhanUtama: string = '';
  getSkrinningPasien() {
    var data: any;
    this.service.get("api/Diagnosa/GetSkrinningPasien/" + this.rd_noreg, data)
      .subscribe(result => {
        this.keluhanUtama = JSON.parse(result).KeluhanUtama;
      });
  }

  // Pemeriksaan
  strPemeriksaanFisik: string = '';
  // Planning in Dignosa
  strPlan: string = '';
  // user_id: string = '';

  // select diagnosa utama 18092019
  kd_icd = '';
  ket_icd = '';
  diagUtamaPilih = '';
  diagUtamaDetailPilih = '';
  onSelectDiagUtama(value) {
    let dataDiagUtama = value["originalObject"];
    dataDiagUtama.TERJEMAHAN.replace("\'", "");
    let dataDiagUtama2 = {
      ...dataDiagUtama,
      TERJEMAHAN: dataDiagUtama.TERJEMAHAN.replace("\'", "")
    }
    this.kd_icd = dataDiagUtama2.ICD_CODE;
    this.ket_icd = dataDiagUtama2.TERJEMAHAN;
    this.diagUtamaPilih = dataDiagUtama2.ListSearch;
    this.strDiagUtama = '';

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN DIAGNOSA UTAMA ( ' + dataDiagUtama2.ListSearch + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveDiagUtama() {
    if (this.strDiagUtama != '') {
      this.ket_icd = this.strDiagUtama;
      this.diagUtamaPilih = this.strDiagUtama;
    }
    console.log(this.ket_icd);
  }

  // new tgl 02-09-2019
  arr_dataDiagnosa = [];
  arr_dataDiagnosaD = [];
  onSelectDiagnosaNew(value) {
    let data = value["originalObject"];
    data.TERJEMAHAN.replace("\'", "");
    let data2 = {
      ...data,
      TERJEMAHAN: data.TERJEMAHAN.replace("\'", "")
    }
    this.arr_dataDiagnosa.push(data2);
    console.log(this.arr_dataDiagnosa);
    this.strDiagnosa = '';
  }
  onSaveTmpDiagnosa() {
    let dt = new Date()
    let value = this.datePipe.transform(dt, 'HH:mm:s');
    if (this.strDiagnosa != '') {
      var diagNoDB = {
        ICD_CODE: value,
        TERJEMAHAN: this.strDiagnosa
      }
      this.arr_dataDiagnosa.push(diagNoDB);
      // console.log(this.arr_dataDiagnosa);
    }
  }
  deleteFieldDiagnosa(index) {
    this.arr_dataDiagnosa.splice(index, 1);
  }
  // new tgl 02-09-2019

  // submit Diagnosa
  dataInsertDiagnosaDetail: any;
  onSubmitAllDiagnosa() {
    // update skrinning di diagnosa
    this.loading = true;

    // skrinning pasien di diagnosa awal
    var paramUpdSkrinning = {
      KeluhanUtama: this.keluhanUtama,
      NoReg: this.rd_noreg
    }
    this.service.put('api/Diagnosa/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinning))
      .subscribe(result => {
        this.keluhanUtama = '';
      },
        error => {
          this.loading = false;
        });

    // insert diagnosa header ke tbl RD_Diagnosa
    var paramInsDiagnosa = {
      RD_NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user
    }
    // api insert RJ Diagnosa
    // console.log('api/Diagnosa/InsertRDDiagnosa', JSON.stringify(paramInsDiagnosa));
    this.service.post('api/Diagnosa/InsertRDDiagnosa', JSON.stringify(paramInsDiagnosa))
      .subscribe(result => {

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'INPUT DATA DIAGNOSA PASIEN',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        // pemeriksaan fisik & planning
        var paramInsPeriksa = {
          PemeriksaanFisik: this.strPemeriksaanFisik,
          Planning: this.strPlan,
          RD_NoReg: this.rd_noreg
        }
        // api update Pemeriksaan Fisik
        this.service.put('api/Diagnosa/UpdatePemeriksaanFisik', JSON.stringify(paramInsPeriksa))
          .subscribe(result => {
            this.strPemeriksaanFisik = '';
            this.strPlan = '';
          },
            error => {
              this.loading = false;
            });
        // pemeriksaan fisik & planning

        // insert diagnosa utama 18-09-2019
        var paraDiagUtama = {
          RD_NoReg: this.rd_noreg,
          Kd_ICD: this.kd_icd,
          Ket_ICD: this.ket_icd
        }
        // api update Pemeriksaan Fisik
        this.service.put('api/Diagnosa/UpdateRDDiagnosa', JSON.stringify(paraDiagUtama))
          .subscribe(result => {
            this.diagUtamaPilih = '';
          },
            error => {
              this.loading = false;
            });

        // insert diagnosa setail ke tbl RD_Diagnosa_Tambahan
        // baru 03-09-2019
        var paramDiagnosaDetail = {
          RD_NoReg: this.rd_noreg,
          UserID: this.useraccessdata.kd_user,
          data: this.arr_dataDiagnosa
        }
        if (this.arr_dataDiagnosa.length != 0) {
          // baru 04-09-2019
          console.log('api/Diagnosa/InsertDiagnosaDetail', JSON.stringify(paramDiagnosaDetail));
          this.service.post('api/Diagnosa/InsertDiagnosaDetail', JSON.stringify(paramDiagnosaDetail))
            .subscribe(result => {
              this.dataInsertDiagnosaDetail = JSON.parse(result);
              this.arr_dataDiagnosa = [];
              this.loading = false;
            },
              error => {
                this.loading = false;
                this.arr_dataDiagnosa = [];
              });
          // baru
        } else {
          this.loading = false;
        }
        // insert diagnosa setail ke tbl RD_Diagnosa_Tambahan
      },
        error => {
          this.loading = false;
        }); // insert diagnosa header ke tbl RD_Diagnosa

    setTimeout(() => {
      this.clearFDiagnosa();
      this.getPasienIGD();
    }, 1000);
  }

  clearFDiagnosa() {
    this.keluhanUtama = '';
    this.strPemeriksaanFisik = '';
    this.subjDetailView = '';
    this.ObjDetailView = '';
    this.diagUtamaView = '';
    this.planDetailView = '';
    this.diagUtamaPilih = '';
    this.strPlan = '';
    this.dataDiagnosaDRiwayat = [];
    this.arr_dataDiagnosa = [];
    this.dataAllDiagnosa = [];
    this.dataDetailResepNRacik = [];
    this.dataDetailResepRacik = [];
  }

  aktif_periksaFisik = false;
  aktif_riwayatDiag = true;
  // Pemeriksaan Fisik
  btnNewPeriksaFisik() {
    this.aktif_periksaFisik = true;
    this.aktif_riwayatDiag = false;
  }

  btnTutupPeriksaFisik() {
    this.aktif_periksaFisik = false;
    this.aktif_riwayatDiag = true;
  }

  val_head = '';
  val_eye = '';
  setNormal() {
    this.val_head = 'Normal';
    this.val_eye = 'Normal';
  }

  // Riwayat Diagnosa
  getAllRiwayatDiagnosaPasienD() {
    this.dataAllDiagnosaD = [];
    this.service.get("api/Diagnosa/GetAllDiagnosaPasien/" + this.kd_rm, '')
      .subscribe(result => {
        this.dataAllDiagnosaD = JSON.parse(result);
      },
        error => {
          this.loading = false;
        });
  }

  keluhanUtamaD: string = '';
  DiagUtamaD: string = '';
  strPemeriksaanFisikD: string = '';
  strPlanD: string = '';
  dataDiagnosaByNoReg: any;

  // tidak diapakai 10-09-2019
  // getSkrinningPasienD() {

  //   this.loading = true;
  //   var data: any;
  //   this.service.get("api/Diagnosa/GetSkrinningPasien/" + this.rd_noreg, data)
  //     .subscribe(result => {
  //         this.keluhanUtamaD = JSON.parse(result).KeluhanUtama;
  //       },
  //       error => {
  //         this.loading = false;
  //       }); // skrinning
  // }

  // tidak diapakai 10-09-2019
  // getDiagnosaByNoReg() { // jadinya fungsi ini utk get periksa fisik dan plan di tbl rd_diagnosa
  //   var data: any;
  //   this.service.httpClientGet('api/Diagnosa/GetDiagnosaPasienByNoReg/' + this.rd_noreg, data)
  //     .subscribe(result => {
  //         data = result;
  //         this.dataDiagnosaByNoReg = data; // lama
  //         this.strPemeriksaanFisikD = this.dataDiagnosaByNoReg.PemeriksaanFisik;
  //         this.strPlanD = this.dataDiagnosaByNoReg.Planning;
  //       },
  //       error => {
  //         this.loading = false;
  //       });
  // }

  // new detail diagnosa
  // tidak diapakai 10-09-2019
  // getDiagnosaDetailByNoReg() {
  //   this.loading = true; // jadinya fungsi ini utk get diagnosa detail
  //   var data: any;
  //   this.service.httpClientGet('api/Diagnosa/GetDiagnosaDetailByNoReg/' + this.rd_noreg, data)
  //     .subscribe(result => {
  //         data = result;
  //         this.dataDiagnosaByNoReg = data;
  //         this.loading = false;
  //       },
  //       error => {
  //         this.loading = false;
  //       });
  // }

  // get keseluruhan detail pasien di halaman diagnosa detail
  onGetAllDiagnosaDetail() {
    this.loading = true;
    var data: any;
    this.resetRiwayatDetailDiag();
    this.service.get("api/Diagnosa/GetSkrinningPasien/" + this.rd_noreg, data)
      .subscribe(result => {
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MELIHAT DETAIL DIAGNOSA PASIEN YANG TELAH DI INPUT',
          Jenis: '2'
        }
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        this.keluhanUtamaD = JSON.parse(result).KeluhanUtama;
        // api untuk menampilkan pemeriksaan fisik dan planning
        this.service.get("api/Diagnosa/GetDiagnosaPasienByNoReg/" + this.rd_noreg, '')
          .subscribe(result => {
            this.strPemeriksaanFisikD = JSON.parse(result).PemeriksaanFisik;
            this.DiagUtamaD = JSON.parse(result).Ket_ICD;
            this.strPlanD = JSON.parse(result).Planning;

            // api get all diagnosa pasien hari ini
            this.service.get("api/Diagnosa/GetDiagnosaDetailByNoReg/" + this.rd_noreg, '')
              .subscribe(result => {
                this.dataDiagnosaByNoReg = JSON.parse(result);
                this.loading = false;
              },
                error => {
                  this.loading = false;
                }); // diagnosa

          },
            error => {
              this.loading = false;
            }); // pemeriksaan fisik & planning

      },
        error => {
          this.loading = false;
        }); // skrinning    
  }

  // lama tidak dipakai
  // diagPilihD: string = '';
  // onSelectDiagnosaDetail(value) {
  // this.dataDiagD = value["originalObject"];
  // this.kd_icd = this.dataDiagD.ICD_CODE;
  // this.ket_icd = this.dataDiagD.TERJEMAHAN;
  // this.diagPilihD = this.dataDiagD.ListSearch;
  // this.user_id = this.dataDiagD.kd_user;
  // }

  onSelectDiagDetailUtama(value) {
    let dataDiagDetailUtama = value["originalObject"];
    dataDiagDetailUtama.TERJEMAHAN.replace("\'", "");
    let dataDiagDetailUtama2 = {
      ...dataDiagDetailUtama,
      TERJEMAHAN: dataDiagDetailUtama.TERJEMAHAN.replace("\'", "")
    }
    this.kd_icd = dataDiagDetailUtama2.ICD_CODE;
    this.ket_icd = dataDiagDetailUtama2.TERJEMAHAN;
    this.diagUtamaDetailPilih = dataDiagDetailUtama2.ListSearch;
    this.strDiagDetailUtama = '';

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN DIAGNOSA UTAMA ( ' + dataDiagDetailUtama2.ListSearch + ' )',
      Jenis: '2'
    }
    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveDiagDetailUtama() {
    if (this.strDiagDetailUtama != '') {
      this.ket_icd = this.strDiagDetailUtama;
      this.diagUtamaDetailPilih = this.strDiagDetailUtama;
    }
  }

  // new tgl 03-09-2019 for diagnosa detail
  onSelectDiagnosaNewDetail(value) {
    let data = value["originalObject"];
    data.TERJEMAHAN.replace("\'", "");
    let data2 = {
      ...data,
      TERJEMAHAN: data.TERJEMAHAN.replace("\'", "")
    }
    this.arr_dataDiagnosaD.push(data2);
    this.strDiagnosaD = '';

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN DIAGNOSA TAMBAHAN ( ' + data.TERJEMAHAN.replace("\'", "") + ' )',
      Jenis: '2'
    }
    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveTmpDiagnosaDetail() {
    let dt = new Date()
    let value = this.datePipe.transform(dt, 'HH:mm:s');
    if (this.strDiagnosaD != '') {
      var diagNoDB = {
        ICD_CODE: value,
        TERJEMAHAN: this.strDiagnosaD
      }
      this.arr_dataDiagnosaD.push(diagNoDB);
      // console.log(this.arr_dataDiagnosaD);
    }
  }
  deleteFieldDiagnosaDetail(index) {
    this.arr_dataDiagnosaD.splice(index, 1);
  }

  aktif_periksaFisikD = false;
  aktif_riwayatDiagD = true;
  // Pemeriksaan Fisik
  btnNewPeriksaFisikD() {
    this.aktif_periksaFisikD = true;
    this.aktif_riwayatDiagD = false;
  }

  btnTutupPeriksaFisikD() {
    this.aktif_periksaFisikD = false;
    this.aktif_riwayatDiagD = true;
  }

  // submit di diagnosa detail
  onSubmitAllDiagnosaDetail() {
    this.loading = true;

    // updt skrinning di detail diagnosa
    var paramUpdSkrinningD = {
      KeluhanUtama: this.keluhanUtamaD,
      NoReg: this.rd_noreg
    }
    this.service.put('api/Diagnosa/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinningD)) // skrinning
      .subscribe(result => {
        this.keluhanUtamaD = '';

        // update diagnosa utama 18-09-2019
        var paraDiagDetailUtama = {
          RD_NoReg: this.rd_noreg,
          Kd_ICD: this.kd_icd,
          Ket_ICD: this.ket_icd
        }
        // api update Pemeriksaan Fisik
        this.service.put('api/Diagnosa/UpdateRDDiagnosa', JSON.stringify(paraDiagDetailUtama))
          .subscribe(result => {
            this.diagUtamaDetailPilih = '';
          },
            error => {
              this.loading = false;
            });

        // diagnosa detail di detail diagnosa
        var paramDiagnosaDetail = {
          RD_NoReg: this.rd_noreg,
          UserID: this.useraccessdata.kd_user,
          data: this.arr_dataDiagnosaD

        }
        if (this.arr_dataDiagnosaD.length != 0) {
          this.service.post('api/Diagnosa/InsertDiagnosaDetail', JSON.stringify(paramDiagnosaDetail))
            .subscribe(result => {
              this.dataInsertDiagnosaDetail = JSON.parse(result);
              this.arr_dataDiagnosaD = [];
              this.loading = false;
            },
              error => {
                this.loading = false;
                this.arr_dataDiagnosaD = [];
              });
        } else {
          this.loading = false;
        }
        // diagnosa detail di detail diagnosa

        // pemeriksaan fisik & planning di detail diagnosa
        var paramInsPeriksa = {
          PemeriksaanFisik: this.strPemeriksaanFisikD,
          Planning: this.strPlanD,
          RD_NoReg: this.rd_noreg
        }
        // api update Pemeriksaan Fisik
        if (this.strPemeriksaanFisikD != '' || this.strPlanD != '') {
          this.service.put('api/Diagnosa/UpdatePemeriksaanFisik', JSON.stringify(paramInsPeriksa))
            .subscribe(result => {
              this.strPemeriksaanFisikD = '';
              this.strPlanD = '';
            },
              error => {
                this.loading = false;
              });
        } else {
          this.loading = false;
        }
        // pemeriksaan fisik & planning di detail diagnosa

        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'UPDATE DATA DIAGNOSA PASIEN',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

      },
        error => {
          this.loading = false;
        }); // updt skrinning di detail diagnosa

    setTimeout(() => {
      this.subjDetailView = '';
      this.ObjDetailView = '';
      this.diagUtamaView = '';
      this.planDetailView = '';
      this.dataAllDiagnosaD = [];
      this.dataDetailResepNRacik = [];
      this.dataDetailResepRacik = [];
      this.getPasienIGD();
    }, 1000);
  }

  dataDiagnosaTTV: any;
  dataDiagnosaHTTV: any;
  bb: string = '';

  // t t v
  getDiagnosaTTV(noreg) {
    this.dataDiagnosaTTV = [];
    this.bb = '';
    this.service.get('api/Diagnosa/GetTTVByNoReg/' + noreg, '')
      .subscribe(result => {
        this.dataDiagnosaTTV = JSON.parse(result);
        for (let i = 0; i < this.dataDiagnosaTTV.length; i++) {
          this.bb = this.dataDiagnosaTTV[i].BB
        }
        this.loading = false;
      },
        error => {
          this.dataDiagnosaTTV = [];
          this.loading = false;
        });
  }
  getDiagnosaHTTV(noreg) {
    this.dataDiagnosaHTTV = [];
    this.service.get('api/Diagnosa/GetTTVByNoReg/' + noreg, '')
      .subscribe(result => {
        this.dataDiagnosaHTTV = JSON.parse(result);
        this.loading = false;
      },
        error => {
          this.dataDiagnosaHTTV = [];
          this.loading = false;
        });
  }
  ttvClear() {
    this.dataDiagnosaHTTV = [];
  }
  // t t v

  onCloseDiagnosaDetail() {
    // this.kd_icd = '';
    // this.ket_icd = '';
    this.strDiagnosaD = '';
    // this.diagPilihD = '';

    this.keluhanUtamaD = '';
    this.strPemeriksaanFisikD = '';
    this.arr_dataDiagnosaD = [];
    this.strPlanD = '';

    this.subjDetailView = '';
    this.ObjDetailView = '';
    this.diagUtamaView = '';
    this.planDetailView = '';
    this.dataDetailResepNRacik = [];
    this.dataDetailResepRacik = []
    this.dataAllDiagnosaD = [];
    this.dataDiagnosaByNoReg = [];
  }

  // ----------------------- END DIAGNOSA ------------------------


  // ---------------------------- START ALKES ------------------------------//
  arr_alkes = [];
  arr_alkes_tmp = [];
  dataAlkesPasien: any;
  getDataAlkesPasien() {
    // console.log("api/AlkesIgd/GetAlkesPasien/" + this.rd_noreg, '');
    this.service.get("api/AlkesIgd/GetAlkesPasien/" + this.rd_noreg, '')
      .subscribe(result => {
        this.dataAlkesPasien = JSON.parse(result);
        console.log(this.dataAlkesPasien);
      },
        error => {
          this.loading = false;
        });
  }

  onSelectAlkesObat(value) {
    let data = value["originalObject"];
    if (this.cara_bayar == '6. FOC') {
      this.arr_alkes.push(data);
      this.strAlkes = '';
    } else {
      this.arr_alkes.push(data);
      this.strAlkes = '';
    }

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN ALKES ( ' + data.Nama_Barang + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  editQty(value, kd) {
    var index = this.arr_alkes.findIndex(x => x.Kode == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.arr_alkes[index].Qty = value;
        this.arr_alkes[index].Total = this.arr_alkes[index].Qty * this.arr_alkes[index].Hrg_Kary;
      } else {
        this.arr_alkes[index].Qty = value;
        this.arr_alkes[index].Total = this.arr_alkes[index].Qty * this.arr_alkes[index].Hrg_Jual;
      }
    }
  }

  deleteFieldAlkesObat(index) {
    this.arr_alkes.splice(index, 1);
  }

  onSubmitAlkes() {
    console.log(this.arr_alkes);
    this.loading = true;
    if (this.cara_bayar == '6. FOC') {
      this.arr_alkes.map(item => {
        return {
          Kode: item.Kode,
          Qty: item.Qty,
          HargaSatuan: item.Hrg_Kary,
          Total: item.Total
        }
      }).forEach(item => this.arr_alkes_tmp.push(item));
    } else {
      this.arr_alkes.map(item => {
        return {
          Kode: item.Kode,
          Qty: item.Qty,
          HargaSatuan: item.Hrg_Jual,
          Total: item.Total
        }
      }).forEach(item => this.arr_alkes_tmp.push(item));
    }

    var paramInsertAlkes = {
      rd_noreg: this.rd_noreg,
      data: this.arr_alkes_tmp
    }
    console.log('api/AlkesIgd/InsAlkesObat_Igd', JSON.stringify(paramInsertAlkes));
    this.service.post('api/AlkesIgd/InsAlkesObat_Igd', JSON.stringify(paramInsertAlkes))
      .subscribe(result => {
        this.arr_alkes = [];
        this.arr_alkes_tmp = [];
        this.getDataAlkesPasien();
        this.loading = false;
      },
        error => {
          this.loading = false;
          this.arr_alkes = [];
          this.arr_alkes_tmp = [];
        });
  }

  deleteAlkesObatPasien(noreg, kdbrg) {
    console.log(noreg, kdbrg);
    this.loading = true;
    var paramDelAlkesObatPasien = {
      noreg: noreg,
      kdbrg: kdbrg
    }
    console.log('api/AlkesIgd/DeleteAlkesObat_Igd', JSON.stringify(paramDelAlkesObatPasien));
    this.service.post('api/AlkesIgd/DeleteAlkesObat_Igd', JSON.stringify(paramDelAlkesObatPasien))
      .subscribe(result => {
        this.getDataAlkesPasien();
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE DATA ALKES',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      },
        error => {
          this.loading = false;
        });
  }
  // ---------------------------- END ALKES ------------------------------//

  /** HLab */
  dataHLab: any;
  dataHLabDetail = [];
  getHasilLab(rm) {
    var param = {
      Kd_RM: rm
    }
    this.service.post('api/AppDokterIgd/GetTrxPasienRM', JSON.stringify(param))
      .subscribe(result => {
        // console.log(result)
        this.dataHLab = JSON.parse(result);
        // this.dataHLab = result
      });
  }

  getHasilLabByTrx(trx) {
    var param = {
      No_Trx: trx
    }
    this.service.post('api/AppDokterIgd/GetHasilLabByTrx', JSON.stringify(param))
      .subscribe(result => {
        this.dataHLabDetail = JSON.parse(result);
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MELIHAT DETAIL HASIL LAB',
          Jenis: '2'
        }
    
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  clearDetailHLab() {
    this.dataHLabDetail = [];
  }

  // PENUNJANG MEDIS
  public filterJenisPlyn: string = "";
  dataJenisPlyn: any;

  getJenisPelayanan() {
    var data: any;
    this.service.httpClientGet('api/AppDokterIgd/GetJenisPelayananPM', data)
      .subscribe(result => {
        data = result;
        this.dataJenisPlyn = data;
      });
  }

  dataListPenunjangMedis: any;
  dataListPaketPM: any;

  getPelPenunjangMedis(noreg) {
    const paramObj = {
      No_Reg: noreg,
      Kd_Dokter: this.useraccessdata.kd_user
    }
    this.service.post('api/AppDokterIgd/GetPenunMedisByNoreg', JSON.stringify(paramObj))
      .subscribe(result => {
        var data: any;
        data = JSON.parse(result);
        // const data = JSON.parse(result);
        this.dataListPenunjangMedis = data;
      },
        err => {
          this.loading = false;
        });
  }

  editDataPemeriksaan = false;
  kd_pmlyn: string = '';
  jenis_plyn: string = '';
  dataPemeriksaanByKdLyn: any;
  selectIsiPenunjangMedis(kdPlyn, jenisPlyn) {
    this.editDataPemeriksaan = false;
    this.kd_pmlyn = kdPlyn;
    this.jenis_plyn = jenisPlyn;
    var paramPeriksa = {
      Kd_Lyn: this.kd_pmlyn,
      Kd_Tarif: "05",
      Kd_Dokter: "99999999"
    }
    this.service.post('api/AppDokterIgd/GetPemeriksaanByKdLyn', JSON.stringify(paramPeriksa))
      .subscribe(result => {
        this.dataPemeriksaanByKdLyn = JSON.parse(result);
        this.getPaketPM();
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MEMILIH JENIS PELAYANAN PENUNJANG MEDIS',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
    setTimeout(() => {
      this.loading = false;
    }, 500)
  }

  trxPM = '';
  arr_editdataPemeriksaan = [];
  editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx) {
    this.trxPM = trx;
    this.selectIsiPenunjangMedis(Kd_Lyn, Jenis_Pelayanan);
    const obj = {
      No_Trx: trx,
      Kd_Dokter: this.useraccessdata.kd_user
    }
    this.service.post('api/AppDokterIgd/GetEditTindMedis', JSON.stringify(obj))
      .subscribe(result => {
        const data = JSON.parse(result);
        this.editDataPemeriksaan = true;
        this.arr_editdataPemeriksaan = data;
        this.getPaketPM();
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MELIHAT DATA PENUNJANG MEDIS YANG TELAH DI INPUT',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.getPaketPM();
        this.editDataPemeriksaan = false;
        this.loading = false;
      });
  }

  public dataTindakan = [];
  arr_dataPemeriksaan = [];
  strTindakan: string = '';
  arrTindJprs = [];
  deletePelPenunjangMedis(notrx) {
    this.loading = true;
    const obj = {
      No_Trx: notrx,
      No_Reg: this.rd_noreg
    }
    this.service.post('api/AppDokterIgd/DelPenunMedis', JSON.stringify(obj))
      .subscribe(result => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.rd_noreg);
        this.getHasilLab(this.kd_rm);
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE DATA PENUNJANG MEDIS',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.rd_noreg);
        this.getHasilLab(this.kd_rm);
        this.loading = false;
      });
  }

  kdkls = '';
  kdlyn = '';
  dataTindakanPasien: any;
  getdataTindPasien(noreg, kdlyn) {
    this.dataTindakanPasien = [];
    this.rd_noreg = noreg;
    this.kdlyn = kdlyn;
    // console.log("api/Tindakan/GetDataTindakanPasien/" + noreg + "/" + kdlyn, '');
    this.service.get("api/AppDokterIgd/GetDataTindakanPasien/" + noreg + "/" + kdlyn, '')
      .subscribe(result => {
        this.dataTindakanPasien = JSON.parse(result);
        // console.log(this.dataTindakanPasien);
      });

    this.service.get('api/AppDokterIgd/GetObjKdKelas/' + noreg, '')
      .subscribe(r => {
        const res = JSON.parse(r);
        this.kdkls = res[0]['KD. KLS'];
      });
  }

  getPaketPM() {
    var data: any;
    const kddok = this.useraccessdata.kd_user;
    const kdlyn = this.kd_pmlyn;
    this.service.httpClientGet('api/AppDokterIgd/GetPaketPMByDok/' + kddok + '/' + kdlyn, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketPM = data;
      },
        err => {
          this.dataListPaketPM = [];
          this.loading = false;
        });
  };

  filterPemeriksaanLyn: string = '';
  clearCariPM() {
    this.filterPemeriksaanLyn = '';
  }

  kd_tindakan: string = '';
  nama_tindakan: string = '';
  dataObjPemeriksaan: any;
  onSelectPemeriksaan(kd_tind, nama) {
    this.kd_tindakan = kd_tind;
    this.nama_tindakan = nama;

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN PEMERIKSAAN PENUNJANG MEDIS ( ' + nama + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    var index = this.dataPemeriksaanByKdLyn.findIndex(x => x.Kd_Tindakan == kd_tind);
    if (index !== -1) {
      this.dataPemeriksaanByKdLyn.splice(index, 1);
    }
    var paramObjPeriksa = {
      Kd_Lyn: this.kd_pmlyn,
      Kd_Tarif: "05",
      Kd_Dokter: "99999999",
      Kd_Tindakan: kd_tind
    }
    this.service.post('api/AppDokterIgd/GetObjPemeriksaanByKdTndk', JSON.stringify(paramObjPeriksa))
      .subscribe(result => {
        const arrDt = JSON.parse(result);
        let price = 0;
        if (this.cara_bayar == '6. FOC') {
          price = 1 * arrDt.Karyawan;
        } else if (this.cara_bayar == '9. BPJS') {
          price = 1 * arrDt.BPJS;
        } else {
          price = 1 * arrDt.Standard;
        }

        const obj = {
          No_Trx: '',
          Kd_Lyn: this.kd_pmlyn,
          No_Reg: this.rd_noreg,
          No_Urut: '',
          Tgl_Periksa: this.useraccessdata.tgl_hari_ini,
          Kd_Dokter_Kirim: this.useraccessdata.kd_user,
          Dokter_Kirim: this.useraccessdata.nama,
          Kd_DrLab: '99999999',
          Kd_PenataLab: '',
          Jumlah: price,
          Discount: '0',
          StatKaryawan: '0',
          FOC: '0',
          UserID: this.useraccessdata.kd_user,
          KD_Kelas: this.kdkls,
          Kunjungan: '',
          Total: price,
          Kd_Tindakan: kd_tind,
          JmlTrx: '1',
          StatCITO: '0',
          TotalH: '0',
          Harga: price
        }
        this.dataObjPemeriksaan = { ...arrDt, ...obj };
        this.arr_dataPemeriksaan.push(this.dataObjPemeriksaan);
      });
  }

  editQtyPenunMedis(qty, kd) {
    const value = qty ? qty : 1;
    var index = this.arr_dataPemeriksaan.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].Karyawan;
        this.arr_dataPemeriksaan[index].StatKaryawan = '1';
        this.arr_dataPemeriksaan[index].FOC = '1';
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].Karyawan;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].Karyawan;
      } else if (this.cara_bayar == '9. BPJS') {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].BPJS;
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].BPJS;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].BPJS;
      } else {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].Standard;
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].Standard;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].Standard;
      }
    }
  }

  deleteFieldPemeriksaan(index, kd_tind) {
    this.dataPemeriksaanByKdLyn.push(this.arr_dataPemeriksaan[index]);
    this.arr_dataPemeriksaan.splice(index, 1);
  }

  deleteTindakanPemeriksaan(kd_tindakan, trx, Kd_Lyn, Jenis_Pelayanan) {
    this.loading = true;
    const obj = {
      No_Trx: trx,
      Kd_Tindakan: kd_tindakan
    }
    this.service.post('api/AppDokterIgd/DelTindakanPenunMedis', JSON.stringify(obj))
      .subscribe(result => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx);
        this.getPaketPM();
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.rd_noreg);
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE PEMERIKSAAN PENUNJANG MEDIS',
          Jenis: '2'
        }
    
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx);
        this.getPaketPM();
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.rd_noreg);
        this.loading = false;
      });
  }

  arrCust: any = []
  SelectPaketPM(item: any) {
    this.loading = true;
    var kddok = this.useraccessdata.kd_user;
    var kdtemp = item;
    this.service.httpClientGet('api/AppDokterIgd/GetPemeriksaanByPaketPM/' + kddok + '/' + kdtemp + '/' + this.kd_pmlyn, '')
      .subscribe(result => {
        this.arrCust = result;
        for (const o of this.arrCust) {
          this.onSelectPemeriksaan(o.Kd_Tindakan, o.Jenis_Pelayanan);
        }
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MENGGUNAKAN PAKET PENUNJANG MEDIS',
          Jenis: '2'
        }
    
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  DelPaketPM(kdtemp) {
    this.loading = true;
    const obj = {
      kd_dokter: this.useraccessdata.kd_user,
      kd_template: kdtemp,
      kd_lyn: this.kd_pmlyn
    }
    this.service.post('api/AppDokterIgd/DelPaketPM', JSON.stringify(obj))
      .subscribe(result => {
        this.getPaketPM();
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE PAKET PENUNJANG MEDIS',
          Jenis: '2'
        }
    
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.loading = false;
      });
  }

  submitLynPenunjangMedis() {
    // this.loading = true;
    const obj = this.arr_dataPemeriksaan.filter(x => x['JmlTrx'] !== '0');
    const today = new Date();
    let currentIndex = 1;
    for (let i = 0; i < obj.length; i++) {
      let unique = '' + currentIndex;
      const m = ('0' + (today.getMonth() + 1)).slice(-2);
      while (unique.length < 4) {
        unique = '0' + unique;
      }
      obj[i].No_Trx = `${this.kd_pmlyn}${today.getFullYear()}${m}${unique}`;
      // currentIndex++
    }
    let objtotal = 0;
    obj.map(x => objtotal += x.Total)
    obj.map(x => x.TotalH = objtotal)

    setTimeout(() => {
      this.service.post('api/AppDokterIgd/InstPenunMedisH', JSON.stringify(obj))
        .subscribe(result => {
          const r = JSON.parse(result)
          if (r.Trx) {
            obj.map(x => x.No_Trx = `${r.Trx}`)
          }
          this.service.post('api/AppDokterIgd/InstPenunMedisD', JSON.stringify(obj))
            .subscribe(result => {
              this.strTindakan = '';
              this.arrTindJprs = [];
              this.dataTindakan = [];
              this.arr_dataPemeriksaan = [];
              this.getdataTindPasien(this.rd_noreg, this.kdlyn);
              this.getPelPenunjangMedis(this.rd_noreg);
              const paramLog = {
                NoReg: this.rd_noreg,
                Kd_Dokter: this.useraccessdata.kd_user,
                Aksi: 'INPUT DATA PENUNJANG MEDIS',
                Jenis: '2'
              }
          
              this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
                .subscribe(result => {
                  this.loading = false;
                },
                  err => {
                    this.loading = false;
                  });
            }, error => {
              this.strTindakan = '';
              this.arrTindJprs = [];
              this.dataTindakan = [];
              this.arr_dataPemeriksaan = [];
              this.getdataTindPasien(this.rd_noreg, this.kdlyn);
              this.getPelPenunjangMedis(this.rd_noreg);
            });
        }, error => {
          this.strTindakan = '';
          this.arrTindJprs = [];
          this.dataTindakan = [];
          this.arr_dataPemeriksaan = [];
          this.getdataTindPasien(this.rd_noreg, this.kdlyn);
          this.getPelPenunjangMedis(this.rd_noreg);
        });
    }, 1000);
  }

  submitUpdtLynPenunjangMedis() {
    this.loading = true;
    (this.trxPM != '' && this.arr_dataPemeriksaan.length > 0)
    const obj = this.arr_dataPemeriksaan.filter(x => x['JmlTrx'] !== '0');
    for (let i = 0; i < obj.length; i++) {
      obj[i].No_Trx = this.trxPM;
    }
    let objtotal = 0;
    obj.map(x => objtotal += x.Total)
    obj.map(x => x.TotalH = objtotal)

    this.service.post('api/AppDokterIgd/UpdtPenunMedisH', JSON.stringify(obj))
      .subscribe(result => {
        this.service.post('api/AppDokterIgd/UpdtPenunMedisD', JSON.stringify(obj))
          .subscribe(result => {
            this.strTindakan = '';
            this.arrTindJprs = [];
            this.dataTindakan = [];
            this.arr_dataPemeriksaan = [];
            this.getdataTindPasien(this.rd_noreg, this.kdlyn);
            this.getPelPenunjangMedis(this.rd_noreg);
            this.loading = false;
            const paramLog = {
              NoReg: this.rd_noreg,
              Kd_Dokter: this.useraccessdata.kd_user,
              Aksi: 'UPDATE DATA PENUNJANG MEDIS',
              Jenis: '2'
            }
        
            this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });
          }, error => {
            this.strTindakan = '';
            this.arrTindJprs = [];
            this.dataTindakan = [];
            this.arr_dataPemeriksaan = [];
            this.getdataTindPasien(this.rd_noreg, this.kdlyn);
            this.getPelPenunjangMedis(this.rd_noreg);
            this.loading = false;
          });
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.rd_noreg);
        this.loading = false;
      });

  }

  dataPaketPM: any;
  dataPostPM: any;
  namaPaketPM: string;
  infoPM: string = null;
  arrCustome = [];
  onSubmitPaketPM() {
    this.loading = true;
    const obj = {
      kd_dokter: this.useraccessdata.kd_user,
      nm_template: this.namaPaketPM,
      kd_poli: this.useraccessdata.kd_poli,
      kd_lyn: this.kd_pmlyn
    }
    if (this.arr_dataPemeriksaan.length > 0) {
      for (var i = 0; i < this.arr_dataPemeriksaan.length; i++) {
        this.arrCustome.push(this.arr_dataPemeriksaan[i]);
      }
    }
    if (this.arr_editdataPemeriksaan.length > 0) {
      for (var i = 0; i < this.arr_editdataPemeriksaan.length; i++) {
        this.arrCustome.push(this.arr_editdataPemeriksaan[i]);
      }
    }
    const arrFil = this.arrCustome;
    this.service.post('api/AppDokterIgd/InstPaketPM', JSON.stringify(obj))
      .subscribe(result => {
        const obj2 = {
          kd_dokter: this.useraccessdata.kd_user,
          data: arrFil,
          kd_poli: this.useraccessdata.kd_poli
        }
        this.service.post('api/AppDokterIgd/InstPaketPMDT', JSON.stringify(obj2))
          .subscribe(result => {
            this.getPaketPM();
            this.namaPaketPM = '';
            this.loading = false;
            const paramLog = {
              NoReg: this.rd_noreg,
              Kd_Dokter: this.useraccessdata.kd_user,
              Aksi: 'MEMBUAT PAKET PENUNJANG MEDIS',
              Jenis: '2'
            }
        
            this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });
          }, error => {
            this.arrCustome = [];
            this.namaPaketPM = '';
            this.loading = false;
          });
      }, error => {
        this.arrCustome = [];
        this.namaPaketPM = '';
        this.getPaketPM();
        this.loading = false;
      });
  }

  onBatalPaketPM() {
    this.namaPaketPM = '';
  }
  // PENUNJANG MEDIS

  // TINDAKAN
  deleteListTindakan(noreg, kdlyn, kdtind) {
    this.loading = true;
    var paramDeleteListTind = {
      noreg: noreg,
      kdlyn: kdlyn,
      kdtind: kdtind
    }
    // console.log('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTind));
    this.service.post('api/AppDokterIgd/DeleteListTindakan', JSON.stringify(paramDeleteListTind))
      .subscribe(result => {
        // console.log(result)
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'DELETE DATA TINDAKAN',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  cito_tind: number = 0;
  disc_tind: number = 0;

  editQtyTindJprs(value, kd) {
    var index = this.arrTindJprs.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_Kary;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_Kary;

      } else if (this.cara_bayar == '9. BPJS') {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_BPJS;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_BPJS;
      } else {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_Std;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_Std;
      }
    }
  }

  editQtyTind(value, kd) {
    var index = this.dataTindakan.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_Kary;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_Kary;

      } else if (this.cara_bayar == '9. BPJS') {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_BPJS;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_BPJS;
      } else {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_Std;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_Std;
      }
    }
  }

  deleteTindakanJprsNew(index, noreg, kdlyn, kdtind) {
    this.loading = true;
    this.arrTindJprs.splice(index, 1);
    var paramDeleteListTindJprs = {
      noreg: noreg,
      kdlyn: kdlyn,
      kdtind: kdtind
    }
    // console.log('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTind));
    this.service.post('api/AppDokterIgd/DeleteListTindakan', JSON.stringify(paramDeleteListTindJprs))
      .subscribe(result => {
        // console.log(result)
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.loading = false;
        const paramLog = {
          NoReg: this.rd_noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'DELETE DATA TINDAKAN',
          Jenis: '2'
        }

        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  deleteFieldTindakan(index) {
    this.dataTindakan.splice(index, 1);
  }

  onSelectTindakan(value) {
    let data = value["originalObject"];
    // console.log('onSelectTindakan data :', data)
    this.dataTindakan.push({ ...data, Qty: '0', Cito: '0', Disc: '0', Harga: '0', Total: '0' });
    this.strTindakan = '';

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'MENAMBAHKAN TINDAKAN ( ' + data.Nama_Tindakan + ' )',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  dataPostTindakan: any;
  onSubmitTindakan() {
    this.loading = true;
    const allTindakan = this.arrTindJprs.concat(this.dataTindakan)
    const allTindakanFilt = allTindakan.filter(x => x['Qty'] !== '0')
    const paramInsertTindakan = {
      NoReg: this.rd_noreg,
      Kd_Lyn: this.useraccessdata.kode_lyn,
      data: allTindakanFilt
    }
    this.service.post('api/AppDokterIgd/PostTindakan', JSON.stringify(paramInsertTindakan))
      .subscribe(result => {
        this.dataPostTindakan = JSON.parse(result);
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.loading = false;
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.getdataTindPasien(this.rd_noreg, this.kdlyn);
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.rd_noreg,
      Kd_Dokter: this.useraccessdata.kd_user,
      Aksi: 'INPUT DATA TINDAKAN',
      Jenis: '2'
    }

    this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // ---------------------------- START PENGKAJIAN ------------------------------//
  // Riwayat pegnkajian
  dataRiwayatPengkajian: any;
  getAllRiwayatPengkajian(kd_rm, noreg) {
    this.loading = true;
    this.service.get("api/Pengkajian/GetRiwayatPengkajian/" + kd_rm, '')
      .subscribe(result => {
        this.dataRiwayatPengkajian = JSON.parse(result);
        this.loading = false;
        const paramLog = {
          NoReg: noreg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MELIHAT RIWAYAT PENGKAJIAN PASIEN',
          Jenis: '2'
        }
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      },
        error => {
          this.loading = false;
        });
  }

  noreg_kaji = '';
  kdrm_kaji = '';
  nama_kaji = '';
  jk_kaji = '';
  tglLahir_kaji = '';
  umur_kaji = '';
  dataDetailPengkajian: any;
  triase = '';
  jalan_nafas_sumbatan_merah = '';
  jalan_nafas_bebas_kuning = '';
  jalan_nafas_ancaman_kuning = '';
  jalan_nafas_bebas_hijau = '';
  pernafasan_henti_nafas_merah = '';
  pernafasan_bradipnea_merah = '';
  pernafasan_sianosis_merah = '';
  pernafasan_takipnea_kuning = '';
  pernafasan_mengi_kuning = '';
  pernafasan_frekuensi_nafas_normal_hijau = '';
  sirkulasi_henti_jantung_merah = '';
  sirkulasi_nadi_tidak_teraba_merah = '';
  sirkulasi_akral_dingin_merah = '';
  sirkulasi_nadi_teraba_lemah_kuning = '';
  sirkulasi_bradikardia_kuning = '';
  sirkulasi_takikardi_kuning = '';
  sirkulasi_pucat_kuning = '';
  sirkulasi_akral_dingin_kuning = '';
  sirkulasi_crt_2_detik_kuning = '';
  sirkulasi_nyeri_dada_kuning = '';
  sirkulasi_nadi_kuat_hijau = '';
  sirkulasi_frekuensi_nadi_normal_hijau = '';

  alasan_penggolongan_triase_abc = '';
  alasan_penggolongan_triase_lainnya = '';
  informasi_didapat_dari_auto_anamnesa = '';
  informasi_didapat_dari_hetero_anamnesa_nama = '';
  informasi_didapat_dari_hetero_anamnesa_hubungan = '';
  cara_masuk = '';
  cara_masuk_lainnya = '';
  asal_masuk_non_rujukan = '';
  asal_masuk_rujukan = '';
  riwayat_penyakit_sekarang = '';
  riwayat_penyakit_dahulu = '';
  riwayat_pengobatan_sebelumnya = '';

  aktifitas_sehari_hari = '';
  nyeri_diprovokasi_oleh = '';
  sifat_nyeri = '';
  lokasi_nyeri = '';
  penjalaran_nyeri = '';
  skala_nyeri = '';
  durasi_nyeri = '';
  frekuensi = '';
  nyeri = '';
  nyeri_hilang_bila_minum_obat = '';
  nyeri_hilang_bila_istirahat = '';
  nyeri_hilang_bila_mendengar_musik = '';
  nyeri_hilang_bila_berubah_posisi = '';
  nyeri_hilang_bila_lain_lain = '';

  sempoyongan = '';
  penopang_duduk = '';
  diberitahukan_ke_dokter = '';
  diberitahukan_ke_dokter_jam = '';

  penurunan_berat_badan = '';
  kesulitan_menerima_makanan = '';
  diagnosa_berhubungan_dengan_gizi = '';

  evaluasi_penurunan_kesadaran = '';
  evaluasi_kejang = '';
  evaluasi_ketidak_efektifan_ronchi = '';
  evaluasi_ketidak_efektifan_wheezing = '';
  evaluasi_ketidak_efektifan_krekels = '';
  evaluasi_ketidak_efektifan_stridor = '';
  evaluasi_sesak_rr = '';
  evaluasi_sesak_ratraksi = '';
  evaluasi_sesak_saturasi = '';

  evaluasi_gangguan_hemodinamik_tekanan_darah = '';
  evaluasi_gangguan_hemodinamik_nadi = '';
  evaluasi_gangguan_hemodinamik_akral = '';
  evaluasi_gangguan_hemodinamik_crt = '';
  evaluasi_gangguan_integritas_kulit_luka = '';
  evaluasi_gangguan_integritas_kulit_gatal = '';
  evaluasi_gangguan_integritas_kulit_merah = '';
  evaluasi_gangguan_keseimbangan_cairan_pendarahan = '';
  evaluasi_gangguan_keseimbangan_cairan_intake = '';
  evaluasi_gangguan_keseimbangan_cairan_output = '';

  risiko_infeksi_nosokomial_rencana_operasi = '';
  risiko_infeksi_nosokomial_cateter_vena_perfifer = '';
  risiko_infeksi_nosokomial_cateter_vena_central = '';
  risiko_infeksi_nosokomial_cateter_urin = '';
  risiko_infeksi_nosokomial_naso = '';
  kategori_penyakit = '';

  skrining_nutrisi_bb = '';
  skrining_nutrisi_tb = '';
  skrining_nutrisi_imt = '';
  skrining_nutrisi_lingkar_kepala = '';
  evaluasi_nyeri = '';
  evaluasi_peningkatan_suhu_tubuh = '';
  evaluasi_lain_lain = '';

  dataPengkajianInfusObat: any;
  dataPengkajianTindakan: any;
  dataPengkajianObservasi: any;
  viewDetailPengkajianPasien(no_reg, kd_rm, nama, jk, tgl_lahir, umur) {

    this.loading = true;
    this.noreg_kaji = no_reg;
    this.kdrm_kaji = kd_rm;
    this.nama_kaji = nama;
    this.jk_kaji = jk;
    this.tglLahir_kaji = tgl_lahir;
    this.umur_kaji = umur;

    console.log("api/Pengkajian/GetRiwayatPengkajianDetail/" + no_reg, '');
    this.service.httpClientGet("api/Pengkajian/GetRiwayatPengkajianDetail/" + no_reg, '')
      .subscribe(result => {
        const paramLog = {
          NoReg: no_reg,
          Kd_Dokter: this.useraccessdata.kd_user,
          Aksi: 'MELIHAT DETAIL PENGKAJIAN PASIEN',
          Jenis: '2'
        }
        this.service.post('api/AppDokterIgd/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        this.dataDetailPengkajian = result;

        this.triase = this.dataDetailPengkajian.triase;
        this.jalan_nafas_sumbatan_merah = this.dataDetailPengkajian['jalan_nafas_sumbatan(merah)'];
        this.jalan_nafas_bebas_kuning = this.dataDetailPengkajian['jalan_nafas_bebas(kuning)'];
        this.jalan_nafas_ancaman_kuning = this.dataDetailPengkajian['jalan_nafas_ancaman(kuning)'];
        this.jalan_nafas_bebas_hijau = this.dataDetailPengkajian['jalan_nafas_bebas(hijau)'];

        this.pernafasan_henti_nafas_merah = this.dataDetailPengkajian['pernafasan_henti_nafas(merah)'];
        this.pernafasan_bradipnea_merah = this.dataDetailPengkajian['pernafasan_bradipnea(merah)'];
        this.pernafasan_sianosis_merah = this.dataDetailPengkajian['pernafasan_sianosis(merah)'];
        this.pernafasan_takipnea_kuning = this.dataDetailPengkajian['pernafasan_takipnea(kuning)'];
        this.pernafasan_mengi_kuning = this.dataDetailPengkajian['pernafasan_mengi(kuning)'];
        this.pernafasan_frekuensi_nafas_normal_hijau = this.dataDetailPengkajian['pernafasan_frekuensi_nafas_normal(hijau)'];

        this.sirkulasi_henti_jantung_merah = this.dataDetailPengkajian['sirkulasi_henti_jantung(merah)'];
        this.sirkulasi_nadi_tidak_teraba_merah = this.dataDetailPengkajian['sirkulasi_nadi_tidak_teraba(merah)'];
        this.sirkulasi_akral_dingin_merah = this.dataDetailPengkajian['sirkulasi_akral_dingin(merah)'];
        this.sirkulasi_nadi_teraba_lemah_kuning = this.dataDetailPengkajian['sirkulasi_nadi_teraba_lemah(kuning)'];
        this.sirkulasi_bradikardia_kuning = this.dataDetailPengkajian['sirkulasi_bradikardia(kuning)'];
        this.sirkulasi_takikardi_kuning = this.dataDetailPengkajian['sirkulasi_takikardi(kuning)'];
        this.sirkulasi_pucat_kuning = this.dataDetailPengkajian['sirkulasi_pucat(kuning)'];
        this.sirkulasi_akral_dingin_kuning = this.dataDetailPengkajian['sirkulasi_akral_dingin(kuning)'];
        this.sirkulasi_crt_2_detik_kuning = this.dataDetailPengkajian['sirkulasi_crt_2_detik(kuning)'];
        this.sirkulasi_nyeri_dada_kuning = this.dataDetailPengkajian['sirkulasi_nyeri_dada(kuning)'];
        this.sirkulasi_nadi_kuat_hijau = this.dataDetailPengkajian['sirkulasi_nadi_kuat(hijau)'];
        this.sirkulasi_frekuensi_nadi_normal_hijau = this.dataDetailPengkajian['sirkulasi_frekuensi_nadi_normal(hijau)'];

        this.alasan_penggolongan_triase_abc = this.dataDetailPengkajian.alasan_penggolongan_triase_abc;
        this.alasan_penggolongan_triase_lainnya = this.dataDetailPengkajian.alasan_penggolongan_triase_lainnya;
        this.informasi_didapat_dari_auto_anamnesa = this.dataDetailPengkajian.informasi_didapat_dari_auto_anamnesa;
        this.informasi_didapat_dari_hetero_anamnesa_nama = this.dataDetailPengkajian.informasi_didapat_dari_hetero_anamnesa_nama;
        this.informasi_didapat_dari_hetero_anamnesa_hubungan = this.dataDetailPengkajian.informasi_didapat_dari_hetero_anamnesa_hubungan;
        this.cara_masuk = this.dataDetailPengkajian.cara_masuk;
        this.cara_masuk_lainnya = this.dataDetailPengkajian.cara_masuk_lainnya;
        this.asal_masuk_non_rujukan = this.dataDetailPengkajian.asal_masuk_non_rujukan;
        this.asal_masuk_rujukan = this.dataDetailPengkajian.asal_masuk_rujukan;
        this.riwayat_penyakit_sekarang = this.dataDetailPengkajian.riwayat_penyakit_sekarang;
        this.riwayat_penyakit_dahulu = this.dataDetailPengkajian.riwayat_penyakit_dahulu;
        this.riwayat_pengobatan_sebelumnya = this.dataDetailPengkajian.riwayat_pengobatan_sebelumnya;

        this.aktifitas_sehari_hari = this.dataDetailPengkajian.aktifitas_sehari_hari;
        this.nyeri_diprovokasi_oleh = this.dataDetailPengkajian.nyeri_diprovokasi_oleh;
        this.sifat_nyeri = this.dataDetailPengkajian.sifat_nyeri;
        this.lokasi_nyeri = this.dataDetailPengkajian.lokasi_nyeri;
        this.penjalaran_nyeri = this.dataDetailPengkajian.penjalaran_nyeri;
        this.skala_nyeri = this.dataDetailPengkajian.skala_nyeri;
        this.durasi_nyeri = this.dataDetailPengkajian.durasi_nyeri;
        this.frekuensi = this.dataDetailPengkajian.frekuensi;
        this.nyeri = this.dataDetailPengkajian.nyeri;

        this.nyeri_hilang_bila_minum_obat = this.dataDetailPengkajian.nyeri_hilang_bila_minum_obat;
        this.nyeri_hilang_bila_istirahat = this.dataDetailPengkajian.nyeri_hilang_bila_istirahat;
        this.nyeri_hilang_bila_mendengar_musik = this.dataDetailPengkajian.nyeri_hilang_bila_mendengar_musik;
        this.nyeri_hilang_bila_berubah_posisi = this.dataDetailPengkajian.nyeri_hilang_bila_berubah_posisi;
        this.nyeri_hilang_bila_lain_lain = this.dataDetailPengkajian.nyeri_hilang_bila_lain_lain;

        this.sempoyongan = this.dataDetailPengkajian.sempoyongan;
        this.penopang_duduk = this.dataDetailPengkajian.penopang_duduk;
        this.diberitahukan_ke_dokter = this.dataDetailPengkajian.diberitahukan_ke_dokter;
        this.diberitahukan_ke_dokter_jam = this.dataDetailPengkajian.diberitahukan_ke_dokter_jam;

        this.penurunan_berat_badan = this.dataDetailPengkajian.penurunan_berat_badan;
        this.kesulitan_menerima_makanan = this.dataDetailPengkajian.kesulitan_menerima_makanan;
        this.diagnosa_berhubungan_dengan_gizi = this.dataDetailPengkajian.diagnosa_berhubungan_dengan_gizi;

        this.evaluasi_penurunan_kesadaran = this.dataDetailPengkajian.evaluasi_penurunan_kesadaran;
        this.evaluasi_kejang = this.dataDetailPengkajian.evaluasi_kejang;

        this.evaluasi_ketidak_efektifan_ronchi = this.dataDetailPengkajian.evaluasi_ketidak_efektifan_ronchi;
        this.evaluasi_ketidak_efektifan_wheezing = this.dataDetailPengkajian.evaluasi_ketidak_efektifan_wheezing;
        this.evaluasi_ketidak_efektifan_krekels = this.dataDetailPengkajian.evaluasi_ketidak_efektifan_krekels;
        this.evaluasi_ketidak_efektifan_stridor = this.dataDetailPengkajian.evaluasi_ketidak_efektifan_stridor;

        this.evaluasi_sesak_rr = this.dataDetailPengkajian.evaluasi_sesak_rr;
        this.evaluasi_sesak_ratraksi = this.dataDetailPengkajian.evaluasi_sesak_ratraksi;
        this.evaluasi_sesak_saturasi = this.dataDetailPengkajian.evaluasi_sesak_saturasi;

        this.evaluasi_nyeri = this.dataDetailPengkajian.evaluasi_nyeri;

        this.evaluasi_gangguan_hemodinamik_tekanan_darah = this.dataDetailPengkajian.evaluasi_gangguan_hemodinamik_tekanan_darah;
        this.evaluasi_gangguan_hemodinamik_nadi = this.dataDetailPengkajian.evaluasi_gangguan_hemodinamik_nadi;
        this.evaluasi_gangguan_hemodinamik_akral = this.dataDetailPengkajian.evaluasi_gangguan_hemodinamik_akral;
        this.evaluasi_gangguan_hemodinamik_crt = this.dataDetailPengkajian.evaluasi_gangguan_hemodinamik_crt;

        this.evaluasi_gangguan_integritas_kulit_luka = this.dataDetailPengkajian.evaluasi_gangguan_integritas_kulit_luka;
        this.evaluasi_gangguan_integritas_kulit_gatal = this.dataDetailPengkajian.evaluasi_gangguan_integritas_kulit_gatal;
        this.evaluasi_gangguan_integritas_kulit_merah = this.dataDetailPengkajian.evaluasi_gangguan_integritas_kulit_merah;

        this.evaluasi_gangguan_keseimbangan_cairan_pendarahan = this.dataDetailPengkajian.evaluasi_gangguan_keseimbangan_cairan_pendarahan;
        this.evaluasi_gangguan_keseimbangan_cairan_intake = this.dataDetailPengkajian.evaluasi_gangguan_keseimbangan_cairan_intake;
        this.evaluasi_gangguan_keseimbangan_cairan_output = this.dataDetailPengkajian.evaluasi_gangguan_keseimbangan_cairan_output;

        this.evaluasi_peningkatan_suhu_tubuh = this.dataDetailPengkajian.evaluasi_peningkatan_suhu_tubuh;
        this.evaluasi_lain_lain = this.dataDetailPengkajian.evaluasi_lain_lain;

        this.risiko_infeksi_nosokomial_rencana_operasi = this.dataDetailPengkajian.risiko_infeksi_nosokomial_rencana_operasi;
        this.risiko_infeksi_nosokomial_cateter_vena_perfifer = this.dataDetailPengkajian.risiko_infeksi_nosokomial_cateter_vena_perfifer;
        this.risiko_infeksi_nosokomial_cateter_vena_central = this.dataDetailPengkajian.risiko_infeksi_nosokomial_cateter_vena_central;
        this.risiko_infeksi_nosokomial_cateter_urin = this.dataDetailPengkajian.risiko_infeksi_nosokomial_cateter_urin;
        this.risiko_infeksi_nosokomial_naso = this.dataDetailPengkajian.risiko_infeksi_nosokomial_naso;
        this.kategori_penyakit = this.dataDetailPengkajian.kategori_penyakit;

        this.skrining_nutrisi_bb = this.dataDetailPengkajian.skrining_nutrisi_bb;
        this.skrining_nutrisi_tb = this.dataDetailPengkajian.skrining_nutrisi_tb;
        this.skrining_nutrisi_imt = this.dataDetailPengkajian.skrining_nutrisi_imt;
        this.skrining_nutrisi_lingkar_kepala = this.dataDetailPengkajian.skrining_nutrisi_lingkar_kepala;

        // this.loading = false;
        // get infus obat
        this.service.get("api/Pengkajian/GetPengkajianIgdInfusObat/" + this.noreg_kaji, '')
          .subscribe(result => {
            this.dataPengkajianInfusObat = JSON.parse(result);

            // get tindakan
            this.service.get("api/Pengkajian/GetPengkajianIgdTindakan/" + this.noreg_kaji, '')
              .subscribe(result => {
                this.dataPengkajianTindakan = JSON.parse(result);

                // get observasi
                this.service.get("api/Pengkajian/GetPengkajianIgdObservasi/" + this.noreg_kaji, '')
                  .subscribe(result => {
                    this.dataPengkajianObservasi = JSON.parse(result);
                    this.loading = false;
                  },
                    error => {
                      this.loading = false;
                    }); // get observasi
              },
                error => {
                  this.loading = false;
                }); // get tindakan
          },
            error => {
              this.loading = false;
            }); // get infus obat
      },
        error => {
          this.loading = false;
        }); // get pengkajian igd
  }
}
// ---------------------------- END PENGKAJIAN ------------------------------//
