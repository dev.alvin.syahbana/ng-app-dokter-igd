import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";

import {TranslateService} from '@ngx-translate/core';

import swal from 'sweetalert2';

@Injectable()
export class AppTranslateLanguage {

    constructor(private translate: TranslateService) {}

    translate_language(){
        this.translate.addLangs(["English", "Bahasa", "France"]);
        this.translate.setDefaultLang('English');

        let browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/English|Bahasa/) ? browserLang : 'English');
    } 
}